#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from flask import Blueprint, render_template, request, redirect, url_for, abort, g, session, flash

from udblab.models import *
from udblab.app import session, Message, mail, celery
from functools import wraps

mod = Blueprint('site', __name__, template_folder='templates')
mac_address = ['2c', '32', '00', 'ff', 'f0']


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session['user'] is None:
            flash("Please Sign In first to access page")
            return redirect(url_for('site.login'))
        return f(*args, **kwargs)

    return decorated_function


@mod.route('/')
def index():
    session['user'] = None
    # send_email.delay()

    return render_template("index.html")


@mod.route('/admin_users')
@login_required
def admin_users():
    users = User.query.filter_by(isAdmin=False).all()
    return render_template("admin_user_table.html", users=users)


@mod.route('/admin_locations', methods=['GET', 'POST'])
# @login_required
def admin_locations():
    if request.method == 'GET':
        loc_query = request.args.get('q')
        if loc_query == 'all':
            locations = Location.query.all()
        else:
            locations = Location.query.filter(Location.road_address.like('%' + loc_query + '%')).all()
        sensors_dic = []
        for location in locations:
            sensors = SensorNode.query.filter(SensorNode.location_id == location.id).all()
            sensors_dic.append(sensors)
        for i in range(len(locations)):
            locations[i].set_sensor_count = len(sensors_dic[i])

        return render_template("admin_location_table.html", locations=locations)

    elif request.method == 'POST':
        location_id = request.form['location_id']
        session['location_id'] = location_id
        return redirect(url_for('site.admin_location_map', location_id=location_id))


@mod.route('/admin_location_map')
@login_required
def admin_location_map():
    location_id = request.args.get('location_id')
    return render_template("admin_location_map.html", user_view=0, location_id=location_id)


@mod.route('/sensor_node')
@login_required
def sensor_node():
    global sensor_temp, sensor_hum
    sensor_node_id = request.args.get('node')
    sensor_node = SensorNode.query.filter_by(id=sensor_node_id).first()
    for sensor in sensor_node.sensors:
        if sensor.sensor_type == SensorType.temperature:
            sensor_temp = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).first()
        elif sensor.sensor_type == SensorType.humidity:
            sensor_hum = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).first()

    history_list = History.query.filter_by(sensor_node_id=sensor_node_id).all()

    return render_template('sensor_node_view.html', sensor_node_id=sensor_node_id, history_list=history_list,
                           temp=sensor_temp.value,
                           hum=sensor_hum.value, sensor_realesed_time=sensor_temp.created_on)


@mod.route('/admin_profile')
def admin_profile():
    user = session['user']
    admin_id = user['id']
    admin = User.query.filter(User.id == admin_id).first()
    user_created_at = admin.created_on
    print(user_created_at)
    return render_template("admin_profile.html", user=admin, user_created_at=user_created_at)


@mod.route('/admin_dashboard', methods=['GET', 'POST', 'PUT'])
@login_required
def admin():
    if request.method == "POST":
        n_username = request.form['username']
        n_email = request.form['email']
        n_password = request.form['password']
        address = request.form['sample4_jibunAddress']
        road_address = request.form['sample4_roadAddress']
        postal_code = request.form['sample4_postcode']
        latitude = request.form['latitude']
        longitude = request.form['longitude']
        node_unique_id = request.form['sensor_node_unique_id']
        n_user_role = request.form.get('user_role')
        if n_user_role == "일반 사용자":
            n_isAdmin = False
        else:
            n_isAdmin = True

        user_db = User(username=n_username, email=n_email, password=n_password, isAdmin=n_isAdmin)
        db.session.add(user_db)
        db.session.commit()
        location_db = Location(address=address, road_address=road_address, postal_code=postal_code, owner_id=user_db.id)
        db.session.add(location_db)
        db.session.commit()
        sensor_node_db = SensorNode(latitude=latitude, longitude=longitude, location_id=location_db.id,
                                    node_unique_id=node_unique_id)
        db.session.add(sensor_node_db)
        db.session.commit()
        # recent_activity = RecentActivities(user_id=user.id, type='user')
        # session_db.add(recent_activity)
        # session_db.commit()
        return redirect(url_for('site.admin_users'))
    elif request.method == "GET":
        user = User.query.filter_by(isAdmin=False).all()
        location = Location.query.all()
        fadebacks = ContactUs.query.all()
        chungju_locs = Location.query.filter(Location.road_address.like('%충주%')).all()
        seoul_locs = Location.query.filter(Location.road_address.like('%서울%')).all()
        chungju_loc_count = len(chungju_locs)
        seoul_loc_count = len(seoul_locs)
        return render_template("new_admin_dashboard.html", chungju_count=chungju_loc_count,
                               seoul_count=seoul_loc_count,
                               user_list=user, location_list=location, fadebacks=fadebacks)


@mod.route('/create_user', methods=['GET', 'POST'])
@login_required
def create_user():
    if request.method == 'POST':
        return render_template("admin_create_user_new.html")


@mod.route('/user', methods=['GET', 'POST', 'DELETE'])
@login_required
def user():
    if request.method == 'GET':
        global user
        data = request.args.get("id")
        user = User.query.filter_by(id=data).first()
        location = Location.query.filter_by(owner_id=user.id).first()
        sensor_node = SensorNode.query.filter_by(location_id=location.id).first()
        return render_template("admin_user_detail_new.html", user=user, location=location, sensor_node=sensor_node)
    elif request.method == 'DELETE':
        id = request.args.get("id")
        user = User.query.filter_by(id=id).first()
        if user.locations.count() > 0:
            location = Location.query.filter(Location.owner_id == user.id).first()
            if location.sensor_nodes.count() > 0:
                sensors = SensorNode.query.filter(SensorNode.location_id == location.id).all()
                for sensor in sensors:
                    db.session.delete(sensor)
            db.session.delete(location)

        db.session.delete(user)
        db.session.commit()
        return ""
    elif request.method == 'POST':
        user_id = request.form['user_id']
        n_username = request.form['username']
        n_email = request.form['email']
        n_password = request.form['password']
        address = request.form['sample4_jibunAddress']
        road_address = request.form['sample4_roadAddress']
        postal_code = request.form['sample4_postcode']
        latitude = request.form['latitude']
        longitude = request.form['longitude']
        node_unique_id = request.form['sensor_node_unique_id']
        n_user_role = request.form.get('user_role')
        if n_user_role == "General User":
            n_isAdmin = False
        else:
            n_isAdmin = True
        user = User.query.filter_by(id=user_id).first()
        user.username = n_username
        user.isAdmin = n_isAdmin
        user.email = n_email
        user.password = n_password
        location = Location.query.filter_by(owner_id=user.id).first()
        location.address = address
        location.road_address = road_address
        location.postal_code = postal_code
        sensor_node = SensorNode.query.filter_by(location_id=location.id).first()
        sensor_node.latitude = latitude
        sensor_node.longitude = longitude
        sensor_node.node_unique_id = node_unique_id
        db.session.commit()
        return render_template("admin_user_detail_new.html", user=user, location=location, sensor_node=sensor_node)


@mod.route('/admin_user_view', methods=['GET'])
@login_required
def admin_user_view():
    if request.method == 'GET':
        id = request.args.get('id')
        user = User.query.filter_by(id=id).first()
        location = Location.query.filter_by(owner_id=user.id).first()
        sensor_node = SensorNode.query.filter_by(location_id=location.id).first()
        sensor_model = SensorModel.query.filter_by(sensor_node_id=sensor_node.id).order_by(
            SensorModel.created_on.desc()).limit(
            1).first()
        return render_template("admin_user_view.html", user=user, location=location,
                               sensor_node=sensor_node,
                               sensor_model=sensor_model)


def generate_user(id):
    user = User.query.filter_by(id=id).first()
    if user.locations.count() > 0:
        locations = Location.query.filter(Location.owner_id == user.id).all()
        for location in locations:
            sensor_nodes = SensorNode.query.filter(SensorNode.location_id == location.id).all()
            location.sensor_nodes = sensor_nodes
        user.locations = locations
    return user


@mod.route('/admin_analysis')
@login_required
def admin_analysis():
    return render_template("admin_analysis.html")


@mod.route('/user_dashboard')
@login_required
def user_dashboard():
    user = session['user']
    # socket_client.start()
    # t = threading.Thread(target=launchServer)
    # if t.is_alive:
    #     print ("Thread is alive")
    #     t.start()
    # else:
    #     t.daemon = True
    #     t.start()
    location = Location.query.filter_by(owner_id=user['id']).first()
    sensor_node = SensorNode.query.filter_by(location_id=location.id).first()
    return render_template("user_dashboard_angular.html", user_view=0, user=user, user_id=user['id'],
                           location_id=location.id, sensor_node_id=sensor_node.id)


@mod.route('/user_profile')
def user_profile():
    user = session['user']
    admin_id = user['id']
    admin = User.query.filter(User.id == admin_id).first()
    created_at = admin.created_on
    return render_template("user_profile.html", user=admin, created_at=created_at)


@mod.route('/user_details')
@login_required
def user_details():
    user = session['user']
    user_id = user['id']
    locations = Location.query.filter(Location.owner_id == user_id).first()
    location_id = locations.id
    return render_template("user_details.html", user_view=0, location_id=location_id)


@mod.route('/user_view')
@login_required
def user_view():
    if request.method == 'GET':
        user = session['user']
        if user['isAdmin']:
            editable_user = User.query.filter(User.id == request.args.get('id')).first()
            session['editable_user'] = editable_user.serialize
            return render_template("user_view.html", user=session['editable_user'], user_view=1)
        else:
            return render_template("user_view.html", user=session['user'], user_view=0)


@mod.route('/user_analysis')
@login_required
def user_analysis():
    return render_template("user_analysis.html")


# @mod.route('/.well-known/assetlinks.json')
# def assetLink():
#     filename = os.path.join(app.static_folder, 'assetlinks.json')
#     with open(filename) as blog_file:
#         data = json.load(blog_file)
#         return jsonify(data)


@mod.route('/messages')
@login_required
def messages():
    return render_template("admin_messages.html")


@mod.route('/login.html', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        attempted_email = request.form['email']
        attempted_password = request.form['password']
        user = User.query.filter_by(email=attempted_email).first()
        if user is not None and user.password == attempted_password:
            session['user'] = user.serialize
            session['username'] = user.username
            if user.isAdmin:
                return redirect(url_for('site.admin'))
            else:
                # connected_clients = socket_client.connected_clients()
                # if connected_clients is not None:
                #     if len(connected_clients) > 0:
                #         for client in connected_clients:
                #             if str(client.getMacAddress()) == str(mac_address):
                #                 print "Matched"
                #                 continue
                return redirect(url_for('site.user_dashboard', user=user.username))
        else:
            return render_template("login.html", error=True)
    return render_template("login.html", error=False)


# def log_out(test, mac_address):
#     global client, mac_addr
#     print
#     test
#     mac_addr = mac_address
#     client = test
#     print
#     mac_addr
#     print
#     "Received client"
#     return mac_address


client = None


@mod.route('/logout')
@login_required
def logout():
    session.clear()
    print("Logging out ----------- ")
    return redirect(url_for('site.index'))
