import socket

import sys

import binascii
import threading

import time

from udblab.app import socketio, app

server = None
mac_address = ['00', '08', 'dc', '52', 'b6', '47']
mc_1 = '00'
connectedClient = []


def comparing_mac_address(buf):
    hexvalue = binascii.hexlify(buf).decode()
    list_buf = [hexvalue[i:i + 2] for i in range(0, len(hexvalue), 2)]
    unicoded_list = list_buf[2:8]
    remote_mac_address = [str(i).strip() for i in unicoded_list]
    return remote_mac_address


def send_success_auth_command(client):
    client.send(bytearray([0x53, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x45]))
    print("Auth success command send")


def send_failure_auth_command(client):
    client.send(bytearray([0x53, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
    print("Auth failure command send")


def send_start_command(client):
    client.send(bytearray([0x53, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x45]))
    print("Start connection send")


def send_check_connection_command(client):
    client.send(bytearray([0x53, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))


def send_on_command():
    if connectedClient is not None and len(connectedClient) > 0:
        connectedClient[0].send(bytearray([0x53, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x45]))
        print("On command sent")
    else:
        print("Client is null")


def send_off_command():
    if connectedClient is not None and len(connectedClient) > 0:
        connectedClient[0].send(bytearray([0x53, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
        print("Off command sent")
    else:
        print("Client is null")


def check_connected_device():
    if connectedClient is not None and len(connectedClient) > 0:
        return True
    else:
        return False


def receive_response_mic(buf):
    global mc_1
    print("Buffer ------------")
    print(buf)
    mc_status_list_data = []
    hexvalue = binascii.hexlify(buf).decode()
    list_buf = [hexvalue[i:i + 2] for i in range(0, len(hexvalue), 2)]
    unicoded_list = list_buf[2:5]
    mc_list = [str(i).strip() for i in unicoded_list]
    print("Mc_list ------------")
    print(mc_list)

    for mc in mc_list:
        if mc == '00':
            mc_1 = 0
        elif mc == '01':
            mc_1 = 1
        mc_status_list_data.append(mc_1)
    return mc_status_list_data


def close_connection(client):
    client.close()
    print("Connection closed")


def open_socket():
    global server
    try:
        addr = ('0.0.0.0', 8080)
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.bind(addr)
        # server.settimeout()
        server.listen(5)
        print("Listening new socketttt ...")
        socketio.emit('response', {
            'type': 'socket',
            "data": "Started"
        }, broadcast=True)
    except socket.error:
        # if server:
        #     server.close()
        print("Could not open socket: " + str())


def handler(client, clientaddr):
    print("Accepted connection from: ", clientaddr)
    while True:
        data = client.recv(1024)
        print("_" + data + "_")
        buf = client.recv(1024)
        if len(buf) > 0:
            remote_mac_address = comparing_mac_address(buf)
            if str(remote_mac_address) != str(mac_address):
                send_success_auth_command(client)
                send_start_command(client)
                processing = 1
                while processing:
                    buff = client.recv(1024)
                    response = receive_response_mic(buff)
                    print(response)
                    with app.test_request_context():
                        socketio.emit('response', {
                            'type': 'on',
                            "data": response
                        }, broadcast=True)
                    time.sleep(10)
                    send_check_connection_command(client)
            else:
                send_failure_auth_command(client)
                close_connection(client)
                break
        else:
            close_connection(client)
            break
    connectedClient.remove(client)


def run_server():
    print("Long running proccess")
    open_socket()
    while True:
        client, addr = server.accept()
        print("Connected client " + str(addr))
        connectedClient.append(client)
        print(connectedClient)
        t = threading.Thread(target=handler, args=(client, addr,))
        t.daemon = True
        t.start()
        socketio.emit('response', {
            'type': 'socket',
            "data": "Started"
        }, broadcast=True)


def start():
    t = threading.Thread(target=run_server())
    t.daemon = True
    t.start()

# if __name__ == "__main__":
#     start()
