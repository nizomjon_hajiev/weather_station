import logging

import sys
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import json
import threading
import time
import binascii

from flask_socketio import emit

from site.routers import logout
# from Queue import Queue
# import Queue
# from queuelib import queue
import requests
import SocketServer

from socket import socket, AF_INET, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR

from udblab.app import socketio, app

connected_controller = set()

connections = set()
client_socket = None
q = None
dd = None
mac_address = ['00', '08', 'dc', '52', 'b6', '47']
# mac_address = ['2c', '32', '00', 'ff']
mc_1 = '00'
mc_2 = '00'
dThreads = {}
isFinished = False
logging.basicConfig(level=logging.DEBUG,
                    format='%(name)s: %(message)s',
                    )


class StoppableThread(threading.Thread):
    def __init__(self, name, target, args=()):
        super(StoppableThread, self).__init__(name=name, target=target, args=args)
        self._status = 'running'
        self.daemon = True

    def stop_me(self):
        if (self._status == 'running'):
            self._status = 'stopping'

    def stopped(self):
        self._status = 'stopped'

    def is_running(self):
        return (self._status == 'running')

    def is_stopping(self):
        return (self._status == 'stopping')

    def is_stopped(self):
        return (self._status == 'stopped')


def StartThread(id):
    """
    Starts a thread and adds an entry to the global dThreads dictionary.
    """
    logging.info('Starting %s' % id)
    dThreads[id] = StoppableThread(name=id, target=incomingConnectionThread,
                                   )
    try:
        dThreads[id].start()
    except:
        print("Thread is alive")
        if client_socket is not None:
            print("Socket available")
            # client_socket.send(bytearray([0x53, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
            # print "Send close connection"
        else:
            print("socket none")


#

def StartSocketThread(id, clientsocket, websocketclient):
    global q
    """
    Starts a thread and adds an entry to the global dThreads dictionary.
    """
    logging.info('Starting %s' % id)

    dThreads[id] = StoppableThread(name=id, target=check_mic_connection,
                                   args=(clientsocket, websocketclient,))
    dThreads[id].start()


class CheckConnectionThread(threading.Thread):
    def __init__(self, socket_client, websocket_client):
        print
        "Starting thread for checking connection"
        threading.Thread.__init__(self)
        self.socket_client = socket_client
        self.websocket_client = websocket_client

    def run(self):
        isDone = False
        while isDone is not True:
            time.sleep(10)
            print("Send check command")
            self.socket_client.send(bytearray([0x53, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
            response = self.socket_client.recv(1024)
            print("Response check command : " + response)
            print("MC_status_list " + str(receive_response_mic(response)))
            data = {
                "type": "mc_status",
                "status": "success",
                "mc_status_list": receive_response_mic(response),
                "message": "Mc status"
            }
            if self.websocket_client is not None:
                try:
                    self.websocket_client.write_message(data)
                except:
                    print("Web socket error")
                    print("Starting tornado")
                    startTornado()


def StopThread(id):
    """
    Stops a thread and removes its entry from the global dThreads dictionary.
    """
    thread = dThreads[id]
    if thread.is_running():
        logging.info('Stopping %s' % id)
        thread.stop_me()
        thread.join()
        logging.info('Stopped %s' % id)
        del dThreads[id]


class MyWebSocketServer(tornado.websocket.WebSocketHandler):
    def open(self):
        connections.add(self)
        print('Connection is available')

    def on_message(self, message):

        print("Default message : " + message)

        if client_socket is not None:
            if message == 'On':
                client_socket.send(bytearray([0x53, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x45]))
                print("Client -----")
                isSent = False
                while isSent is not True:
                    socket_message = client_socket.recv(1024)
                    print(socket_message)
                    print("Data received ---> ")
                    mc_status_list_on = receive_response_on(socket_message)
                    print(mc_status_list_on)
                    isSent = True
                    data = {
                        "type": "On",
                        "status": "success",
                        "mc_status_list_on": mc_status_list_on,
                        "message": message
                    }
                    print('Message on received: %s' % message)
                    self.write_message(json.dumps(data))
            elif message == 'Off':
                client_socket.send(bytearray([0x53, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
                isSent = False
                while isSent is not True:
                    socket_message = client_socket.recv(1024)
                    print(socket_message)
                    mc_status_list_off = receive_response_off(socket_message)
                    print(mc_status_list_off)
                    isSent = True
                    data = {
                        "type": "Off",
                        "status": "success",
                        "mc_status_list_off": mc_status_list_off,
                        "message": message
                    }
                    print('Message off received: %s' % message)
                    self.write_message(json.dumps(data))
            elif message == 'close':
                close()
        else:
            data = {
                "type": "connection",
                "status": "error"
            }
            self.write_message(json.dumps(data))

    def on_close(self):
        connections.remove(self)
        close_conn()
        print('Connection closed')
        # sys.exit()

    def check_origin(self, origin):
        return True


def receive_response_mic(buf):
    mc_1_status = "00"
    print("Buffer ------------")
    print(buf)
    mc_status_list_data = []
    hexvalue = binascii.hexlify(buf).decode()
    list_buf = [hexvalue[i:i + 2] for i in range(0, len(hexvalue), 2)]
    unicoded_list = list_buf[2:5]
    mc_list = [str(i).strip() for i in unicoded_list]
    print("Mc_list __--- ------")
    print(mc_list)

    for mc in mc_list:
        if mc == '00':
            mc_1_status = 0
        elif mc == '01':
            mc_1_status = 1
        mc_status_list_data.append(mc_1_status)
    return mc_status_list_data


def receive_response_on(buf):
    mc_1_on = "00"
    print("Buffer ------------")
    print(buf)
    mc_status_list_data = []
    hexvalue = binascii.hexlify(buf).decode()
    list_buf = [hexvalue[i:i + 2] for i in range(0, len(hexvalue), 2)]
    unicoded_list = list_buf[2:5]
    mc_list = [str(i).strip() for i in unicoded_list]
    print("Mc_list __--- ------")
    print(mc_list)

    for mc in mc_list:
        if mc == '00':
            mc_1_on = 0
        elif mc == '01':
            mc_1_on = 1
        mc_status_list_data.append(mc_1_on)
    return mc_status_list_data


def receive_response_off(buf):
    mc_2_off = "00"
    print("Buffer ------------")
    print(buf)
    mc_status_list_data = []
    hexvalue = binascii.hexlify(buf).decode()
    list_buf = [hexvalue[i:i + 2] for i in range(0, len(hexvalue), 2)]
    unicoded_list = list_buf[2:5]
    mc_list = [str(i).strip() for i in unicoded_list]
    print("Mc_list __---------")
    print(mc_list)

    for mc in mc_list:
        if mc == '00':
            mc_2_off = 0
        elif mc == '01':
            mc_2_off = 1
        mc_status_list_data.append(mc_2_off)
    return mc_status_list_data


def addToList(client_socket, mac_address):
    global connected_controller, q, dd
    io_controller = IOController(client_socket, mac_address)
    connected_controller.add(io_controller)
    print(connected_controller)


def check_mic_connection(client, websocket_client):
    while True:
        response = client.recv(1024)
        print("Response : " + response)
        print("MC_status_list " + str(receive_response_mic(response)))
        data = {
            "type": "mc_status",
            "status": "success",
            "mc_status_list": receive_response_mic(response),
            "message": "Mc status"
        }
        if websocket_client is not None:
            try:
                websocket_client.write_message(data)
            except:
                print("Web socket error")
                print("Starting tornado")
                startTornado()
        time.sleep(5)
        client.send(bytearray([0x53, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))


def handler(clientsocket):
    global connected_controller, isFinished, client
    buf = client_socket.recv(1024)
    if len(buf) > 0:
        remote_mac_address = comparing_mac_address(buf)
        print(connected_controller)
        with app.test_request_context():
            socketio.emit("connected_client", {'data': str(remote_mac_address)}, namespace='/test')
        if True:
            print(remote_mac_address)
            print("Accepted connection from: ", remote_mac_address)
            clientsocket.send(bytearray([0x53, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x45]))
            print("Send Connection Success command")
            client_socket.send(bytearray([0x53, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x45]))
            print("Start command send")
            for client in connections:
                print(client)
                data = {
                    "type": "connected_client",
                    "mac_address": remote_mac_address,
                    "status": "success"
                }
                print(data)
                client.write_message(data)
            while True:
                response = client_socket.recv(1024)
                print("Response : " + response)
                print("MC_status_list " + str(receive_response_mic(response)))
                data = {
                    "type": "mc_status",
                    "status": "success",
                    "mc_status_list": receive_response_mic(response),
                    "message": "Mc status"
                }
                if client is not None:
                    try:
                        client.write_message(data)
                    except:
                        print("Web socket error")
                        print("Starting tornado")
                        startTornado()
                time.sleep(10)
                client_socket.send(bytearray([0x53, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))

        else:
            print("Wrong mac_address")
            clientsocket.send(bytearray([0x53, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
            clientsocket.shutdown(1)
            clientsocket.close()
    else:
        clientsocket.send(bytearray([0x53, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
        clientsocket.shutdown(1)
        clientsocket.close()
        # client.write_message(str(connected_controller))


application = tornado.web.Application([(r'/websocketserver', MyWebSocketServer), ])


def incomingConnectionThread():
    global client_socket, isFinished, serversocket
    addr = ('0.0.0.0', 8080)
    try:
        serversocket = socket()
        serversocket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    except:
        print("Errrorrrrr")
    try:
        serversocket.bind(addr)
        serversocket.listen(1)
        print("Server is listening for connections\n")
        while isFinished is not True:
            time.sleep(1)
            clientsocket, clientaddr = serversocket.accept()
            client_socket = clientsocket
            handler(clientsocket)

    except KeyboardInterrupt:
        print("Closing server socket...")
        serversocket.close()
    except Exception as msg:
        print("Closing server socket ffff..." + str(msg))
        if client_socket is not None:
            handler(client_socket)
            # serversocket.shutdown(1)
            # serversocket.close()


def comparing_mac_address(buf):
    hexvalue = binascii.hexlify(buf).decode()
    list_buf = [hexvalue[i:i + 2] for i in range(0, len(hexvalue), 2)]
    unicoded_list = list_buf[2:8]
    remote_mac_address = [str(i).strip() for i in unicoded_list]
    return remote_mac_address


def start():
    logging.basicConfig(format='%(threadName)s:%(asctime)s:%(levelname)s:%(message)s',
                        stream=sys.stdout, level=logging.INFO)
    StartThread("incoming_thread")
    startTornado()


def check_connection_for_mobile():
    global client_socket


def startTornado():
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(9007)
    tornado.ioloop.IOLoop.instance().start()


def stopTornado():
    tornado.ioloop.IOLoop.instance().stop()


def close_conn():
    global isFinished
    if client_socket is not None:
        client_socket.send(bytearray([0x53, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
        print("Sending close command")
        # client_socket.close()
        # stopTornado()
        if isFinished is False:
            isFinished = True
        print("Socket closed")
    else:
        print("Error socket none")


def close():
    global isFinished
    if client_socket is not None:
        client_socket.send(bytearray([0x53, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
        client_socket.close()
        if isFinished is False:
            isFinished = True
        logout()
        print("Socket closed")
    else:
        print("Error socket none")
        # isFinished = True
        # # StopThread("socket_thread")
        # # StopThread("incoming_thread")
        # time.sleep(2)
        # stopTornado()


class IOController():
    def __init__(self, socket_client, mac_address):
        self.socket_client = socket_client
        self.mac_address = mac_address

    def getMacAddress(self):
        return self.mac_address

    def getSocketClient(self):
        return self.socket_client

# @socketio.on('connect', namespace='/test')
# def test_connect():
#     print "urarrraa"
#     emit('my_response', {'data': 'Connected'})
