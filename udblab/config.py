import os

basedir = os.path.dirname(os.path.realpath(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'snowMelting.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')