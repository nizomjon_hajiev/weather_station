/**
 * Created by Nizomjon on 8/6/2017.
 */
var echo_service, socket;
app.controller('MainCtrl', ['$scope', '$log', '$http', '$interval', '$location', function ($scope, $log, $http, $interval, $location) {
    $scope.name = 'Main';
    $log.info($location.path());
    $scope.activeMenu = 'main';
    $log.info($scope.activeMenu);
    control_panel();
    $scope.node_id = $('#sensor_node_id').val();
    // $http({
    //     url: '/api/v1/check_mic_status/',
    //     method: "GET"
    // }).then(function (success) {
    //     $log.info(success.data);
    //     var mc_status_list = success.data.result;
    //     var mc_1 = mc_status_list[0];
    //     $log.info(mc_1['mic_1']);
    //     var mc_2 = mc_status_list[1];
    //     $log.info(mc_2);
    //     var mc_3 = mc_status_list[2];
    //     $log.info(mc_3);
    //
    //     mcdata(mc_1['mic_1'], mc_2['mic_2'], mc_3['mic_3'])
    // }, function (error) {
    //     $log.info(error)
    // });
    $http({
        url: '/api/v1/last_seen/' + $scope.node_id + '/',
        method: "GET"
    }).then(function (success) {
        $log.info(success.data);
        var last_seen_data = success.data;
        load_last_seen_data(last_seen_data.last_temp, last_seen_data.last_hum, last_seen_data.last_wind,
            last_seen_data.created_on);
        OnOffBtnPanel($http, $scope.node_id);
    }, function (error) {
        $log.info(error)
    });
    socket = io.connect('http://' + document.domain + ':' + location.port + '/test');
    socket.on('connect', function () {
        // we emit a connected message to let knwo the client that we are connected.
        append("Connection opennn");
        socket.emit('client_connected', {data: 'New client!'});
        socket.emit("check_device", {data: 'Off'});

    }).on('response', showMsg);

    // socket.on('heating_on')

    function showMsg(msg) {
        if (msg['type'] === 'check_device') {
            append(msg['data'])
        }
        append(msg)
    }

    append = function (text) {
        document.getElementById("eventi_websocket").insertAdjacentHTML('beforeend',
            "\t" + text + "\n");
    };
    // var WebSocket = require('ws')
    // echo_service = new WebSocket('ws://138.197.126.15:9007/websocketserver');
    // echo_service.onmessage = function (event) {
    //     var data = JSON.parse(event.data);
    //     // echo_service.send("good");
    //     console.log(data);
    //     append("\tMessage received: " + event.data);
    //     if (data["status"] === 'error') {
    //         $("#connected_IO").attr({"style": "visibility: visible", "class": "alert alert-warning"});
    //         $("#warning").text("Warning");
    //         // $("#connected_IO").innerHTML = '<strong>Warning</strong>';
    //         $('#io_address').text("Waiting client");
    //         document.getElementById("onBtn").disabled = true;
    //         document.getElementById("offBtn").disabled = true;
    //     } else if (data['status'] === 'success') {
    //         if (data['type'] === 'connected_client') {
    //             $("#connected_IO").attr({"style": "visibility: visible", "class": "alert alert-success"});
    //             $("#warning").text("Success");
    //             // $("#connected_IO").prepend("<strong id='success'>Success</strong>");
    //             $('#io_address').text("Mac Address : " + data['mac_address']);
    //             // var mc_status_list_start = response.data.result['mc_status_list'];
    //             // mcdata(1, mc_status_list_start[0], mc_status_list_start[1], mc_status_list_start[2])
    //             // $interval.cancel(update_client);
    //             document.getElementById("onBtn").disabled = false;
    //             document.getElementById("offBtn").disabled = false;
    //         } else if (data['type'] === 'On') {
    //             ControlPanel(1);
    //         } else if (data['type'] === 'Off') {
    //             ControlPanel(0);
    //         } else if (data["type"] === 'mc_status') {
    //             var mc_status_list = data['mc_status_list'];
    //             mcdata(mc_status_list[0], mc_status_list[1], mc_status_list[2])
    //             console.log(data['message']);
    //         }
    //     }
    //     //alert(event.data);
    //     //echo_service.close();
    // };
    // echo_service.onopen = function () {
    //     append("Connection open");
    //     echo_service.send("hello!");
    //
    //
    // };
    // echo_service.onclose = function () {
    //     append("\tConnection close");
    // };
    // echo_service.onerror = function () {
    //     append("\tConnection error");
    // };

    var update_sensor_data = function () {
        $http({
            url: '/api/v1/sensor_models/' + $scope.node_id + '/',
            method: 'GET'
        }).then(function (response) {
            $log.info(response.data)
            $scope.temp = response.data.temperature;
            $scope.humidity = response.data.humidity;
            $scope.wind_speed = response.data.wind_speed;
        })
    };

    var update_connected_client = function () {
        $http({
            url: '/api/v1/connected_client/',
            method: 'GET'
        }).then(function (response) {
            console.log(response.data);
            if (response.data.result['type'] === 'error') {
                $("#connected_IO").attr({"style": "visibility: visible", "class": "alert alert-warning"});
                $("#warning").text("Warning");
                // $("#connected_IO").innerHTML = '<strong>Warning</strong>';
                $('#io_address').text("Waiting client");
                document.getElementById("onBtn").disabled = true;
                document.getElementById("offBtn").disabled = true;

            } else {
                $("#connected_IO").attr({"style": "visibility: visible", "class": "alert alert-success"});
                $("#warning").text("Success");
                // $("#connected_IO").prepend("<strong id='success'>Success</strong>");
                $('#io_address').text("Mac Address : " + response.data.result['mac_address']);
                // var mc_status_list_start = response.data.result['mc_status_list'];
                // mcdata(1, mc_status_list_start[0], mc_status_list_start[1], mc_status_list_start[2])
                // $interval.cancel(update_client);
                document.getElementById("onBtn").disabled = false;
                document.getElementById("offBtn").disabled = false;

            }
        });
    };


    $scope.isActive = function (viewLocation) {
        $log.info(viewLocation);
        return viewLocation === $location.path();
    };


    $interval(update_sensor_data, 1000);

    $('#logout').click(function () {
        console.log("Logoutttt");
        echo_service.send("close");
    })


}]);


function initSocketIo() {

}

function log_out() {
    $('#myModal').modal('show');
}

function load_last_seen_data(temp, hum, wind, date) {
    var last_temp = temp;
    var last_hum = hum;
    var last_wind = wind;
    var last_seen_date = date;
    last_seen_date = last_seen_date.substring(0.20).split(' ');
    last_seen_date = last_seen_date[1] + ' ' + last_seen_date[2] + ' ' + last_seen_date[3] + ' ' + last_seen_date[4];
    $('#last_seen_container').html('');
    var html =
        '<h3><span>마지막으로 본 날짜: </span>' + last_seen_date + '</h3>' +
        '<div class="col-md-4">' +
        '<div id="hum" style="width:300px; height:280px"></div>' +
        '</div>' +
        '<div class="col-md-4">' +
        '<div id="temp" style="width:300px; height:280px"></div>' +
        '</div>' +
        '<div class="col-md-4">' +
        '<div id="wind_speed" style="width:300px; height:280px"></div>' +
        '</div>';
    $('#last_seen_container').html(html);
    var temp = new JustGage({
        id: "temp",
        value: last_hum,
        min: -10,
        max: 100,
        title: "온도"
    });
    var hum = new JustGage({
        id: "hum",
        value: last_temp,
        min: 0,
        max: 60,
        customSectors: {
            percents: true,
            ranges: [{
                color: "#43bf58",
                lo: 0,
                hi: 50
            }, {
                color: "#ff3b30",
                lo: 51,
                hi: 100
            }]
        },
        title: "습도 %"
    });
    var wind_speed = new JustGage({
        id: "wind_speed",
        value: last_wind,
        min: 0,
        max: 10,
        title: "바랍 속도m/s"
    });
    console.log("New chart")
}

var timerVar;

function OnOffBtnPanel($http, sensor_node_id) {
    var isRunning = false;
    var C_P;
    var MC_1;
    var MC_2;
    var MC_3;
    var start_time;
    var end_time;
    var calculation_time;
    var datetime;
    var totalSeconds;
    $('#heating_controller_container button').click(function (e) {
        e.preventDefault();
        console.log('Clicked');
        if ($(this).hasClass('unlocked')) {
            if (!isRunning) {
                $('#offBtn').toggleClass('btn-info btn-default');
                $('#onBtn').toggleClass('btn-default btn-info');
                $('#switch_status').attr('class', 'alert alert-danger').html('켜져 있음.');
                $('#heating_container').append("<div id='timer' align='center' style='margin: 20px;'></div>");
                isRunning = true;
                // countup();
                timerVar = setInterval(countTimer, 1000);
                totalSeconds = 0;

                function countTimer() {
                    ++totalSeconds;
                    var hour = Math.floor(totalSeconds / 3600);
                    var minute = Math.floor((totalSeconds - hour * 3600) / 60);
                    var seconds = totalSeconds - (hour * 3600 + minute * 60);
                    console.log("tootal second : " + totalSeconds);
                    document.getElementById("timer").innerHTML = hour + ":" + minute + ":" + seconds;
                }

                start_time = new Date().getTime();
                var currentdate = new Date();
                datetime = currentdate.getFullYear() + "-"
                    + ((currentdate.getMonth() + 1) < 10 ? '0' : '') + (currentdate.getMonth() + 1) + "-"
                    + currentdate.getDate() + " "
                    + currentdate.getHours() + ":"
                    + (currentdate.getMinutes() < 10 ? '0' : '') + currentdate.getMinutes() + ":"
                    + currentdate.getSeconds();
                console.log(datetime);
                if (socket !== null) {

                    socket.emit("On", {data: 'On'});
                    console.log("On Message sent")
                } else {
                    console.log("On message error")
                }

            }
        }

        if ($(this).hasClass('locked')) {
            if (isRunning) {
                $('#offBtn').toggleClass(' btn-default btn-info');
                $('#onBtn').toggleClass(' btn-info btn-default');
                $('#switch_status').attr('class', 'alert alert-danger').html('켜져 있음.');
                $('#switch_status').attr('class', 'alert alert-info').html('꺼져 있음');
                $('#timer').remove();
                isRunning = false;
                // countstop();
                clearInterval(timerVar);
                end_time = new Date().getTime();
                var currentdate1 = new Date();
                var datetime1 = currentdate1.getFullYear() + "-"
                    + ((currentdate1.getMonth() + 1) < 10 ? '0' : '') + (currentdate1.getMonth() + 1) + "-"
                    + currentdate1.getDate() + " "
                    + currentdate1.getHours() + ":"
                    + (currentdate1.getMinutes() < 10 ? '0' : '') + currentdate1.getMinutes() + ":"
                    + currentdate1.getSeconds();

                console.log(datetime1);
                calculation_time = end_time - start_time;
                console.log(calculation_time);
                var numseconds = Math.round(calculation_time / 1000);
                console.log(numseconds);
                var calculated_time = moment.utc(moment(datetime1, "DD/MM/YYYY HH:mm:ss").diff(moment(datetime, "DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
                console.log(calculated_time);
                failedCaclulatedTime = calculated_time;
                if (socket !== null) {
                    socket.emit("Off", {data: 'Off'});
                    console.log("Off message sent")
                } else {
                    console.log("Off message error")
                }
            }
        }
    });

}

function mcdata(MC_1, MC_2, MC_3) {

    var color_style_1;
    var color_style_2;
    var color_style_3;

    if (MC_1 === 0) {
        color_style_1 = "red";
    } else if (MC_1 === 1) {
        color_style_1 = "green";
    }
    if (MC_2 === 0) {
        color_style_2 = "red";
    } else if (MC_2 === 1) {
        color_style_2 = "green";
    }
    if (MC_3 === 0) {
        color_style_3 = "red";
    } else if (MC_3 === 1) {
        color_style_3 = "green";
    }
    var c = document.getElementById("myControl");
    var ms1 = c.getContext("2d");
    var ms2 = c.getContext("2d");
    var ms3 = c.getContext("2d");
    var ms4 = c.getContext("2d");

    ms1.beginPath();
    ms1.font = "14px Arial black";
    ms1.fillStyle = "black";
    ms1.fillText("MC_1 ", 40, 105);
    ms1.arc(50, 75, 15, 0, 2 * Math.PI);
    ms1.fillStyle = color_style_1;
    ms1.fill();
    ms1.moveTo(50, 60);
    ms1.lineTo(50, 45);
    ms1.stroke();

    ms2.beginPath();
    ms2.font = "14px Arial black";
    ms2.fillStyle = "black";
    ms2.fillText("MC_2 ", 140, 105);
    ms2.arc(150, 75, 15, 0, 2 * Math.PI);
    ms2.fillStyle = color_style_2;
    ms2.fill();
    ms2.moveTo(150, 60);
    ms2.lineTo(150, 45);
    ms2.stroke();

    ms3.beginPath();
    ms3.font = "14px Arial black";
    ms3.fillStyle = "black";
    ms3.fillText("MC_3 ", 240, 105);
    ms3.arc(250, 75, 15, 0, 2 * Math.PI);
    ms3.fillStyle = color_style_3;
    ms3.fill();
    ms3.moveTo(250, 60);
    ms3.lineTo(250, 45);
    ms3.stroke();

    ms4.beginPath();
    ms4.moveTo(50, 45);
    ms4.lineTo(250, 45);
    ms4.stroke();
}

function ControlPanel(C_P) {
    var color_style;
    var c = document.getElementById("myControl");
    var mc = c.getContext("2d");
    if (C_P === 0) {
        color_style = "red"
    } else if (C_P === 1) {
        color_style = "green"

    }
    mc.beginPath();
    mc.font = "16px Arial black";
    mc.fillStyle = "black";
    mc.fillText("Control panel", 0, 15);
    mc.fillText("C_P", 175, 30);
    mc.arc(150, 20, 15, 0, 2 * Math.PI);
    mc.fillStyle = color_style;
    mc.moveTo(150, 35);
    mc.lineTo(150, 45);
    mc.fill();
    mc.stroke();

}

