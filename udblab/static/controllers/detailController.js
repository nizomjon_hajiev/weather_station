/**
 * Created by Nizomjon on 8/6/2017.
 */
app.controller('DetailCtrl', ['$scope', '$log', '$http', '$location', function ($scope, $log, $http, $location) {
    $scope.name = 'Main';
    $log.info($location.path());

    $http({
        url: '/api/v1/history/',
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        data: JSON.stringify({
            'id':
                $('#sensor_node_id').val(),
            'type': 'post'
        })
    }).then(function (success) {
        $log.info(success.data);
        load_history_table(success.data)
    });
    $scope.start_time = getCurrentDate(true);
    $scope.end_time = getCurrentDate(false);


    $scope.load_data = function () {

        // $scope.start_time = $('#start_time').val();
        // $scope.end_time = $('#end_time').val();

        console.log("Start_time : " + $('#start_time').val());
        console.log("End_time : " + $('#end_time').val());
        $http({
            url: '/api/v1/filter_by_date/',
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            data: JSON.stringify({
                'start_time': $('#start_time').val(),
                'end_time': $('#end_time').val(),
                'sensor_node_id': $('#sensor_node_id').val()
            })
        }).then(function (response) {
            $log.info(response.data);
            var data = response.data;
            drawGraph(data.temperature_list, data.humidity_list, data.wind_speed_list, '',
                data.temperature_list_date, data.humidity_list_date, data.wind_speed_list_date, '')
        })

    };
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}]);

function getCurrentDate(isStartTime) {
    var today = new Date();
    var dd = today.getDate();
    if (isStartTime) {
        if (dd > 10) {
            dd = today.getDate() - 10;
        }
    }
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return yyyy + '-' + mm + '-' + dd
}

function load_history_table(data) {
    console.log(data);
    var history_list = data;
    var html = '';
    var j;
    if (history_list.length > 10) {
        j = 10;
    }
    else {
        j = history_list.length
    }
    for (var i = 0; i < j; i++) {
        var calc_time = Math.floor(history_list[i].calculated_time);
        var hour = 0;
        var minute = 0;
        var second = 0;
        var calculated_td_html = '';
        if (calc_time > 3600) {
            hour = parseInt(calc_time / 3600);
            minute = parseInt((calc_time % 3600) / 60);
            second = parseInt(calc_time % 60);
            calculated_td_html = '<td>' + hour + ' hour ' + minute + ' minute ' + second + ' second</td>'
        } else if ((calc_time < 3600) && (calc_time > 60)) {
            minute = parseInt((calc_time % 3600) / 60);
            second = parseInt(calc_time % 60);
            calculated_td_html = '<td>' + minute + ' minute ' + second + ' second</td>'
        } else {
            calculated_td_html = '<td>' + calc_time + ' second</td>'
        }
        var trHtml = '<tr align="CENTER">' +
            '<td>' + history_list[i].start_time + '</td>' +
            '<td>' + history_list[i].end_time + '</td>' +
            calculated_td_html +
            '</tr>'
        html += trHtml
    }
    $('#history_tbody').html(html);

}

function searchBtn() {
    // var sensor_node_id = $('#sensor_node_id').val();
    $scope.$apply(function () {

        $scope.start_time = $('#start_time').val();
        $scope.end_time = $('#end_time').val();

        console.log("Start_time : " + $scope.start_time);
        console.log("End_time : " + $scope.start_time);
        $http({
            url: '/api/v1/filter_by_date/',
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            data: JSON.stringify({
                'start_time': $scope.start_time,
                'end_time': $scope.end_time,
                'sensor_node_id': $('#sensor_node_id').val()
            })
        }).then(function (response) {
            $log.info(response.data);
            var data = response.data;
            drawGraph(data.temperature_list, data.humidity_list, data.wind_speed_list, '',
                data.temperature_list_date, data.humidity_list_date, data.wind_speed_list_date, '')
        })

    });


}

function drawGraph(temp_list, hum_list, wind_list, pressure_list, temp_list_date, hum_list_date, wind_list_date, pressure_list_date) {
    // bar chart data
    $('#temp_chart').remove();
    $('#hum_chart').remove();
    $('#wind_chart').remove();
    $('#pressure_chart').remove();
    var tempChart = document.createElement('canvas');
    tempChart.id = 'temp_chart';
    tempChart.width = 400;
    tempChart.height = 300;
    var humChart = document.createElement('canvas');
    humChart.id = 'hum_chart';
    humChart.width = 400;
    humChart.height = 300;
    var windChart = document.createElement('canvas');
    windChart.id = 'wind_chart';
    windChart.width = 400;
    windChart.height = 300;
    var pressureChart = document.createElement('canvas');
    pressureChart.id = 'pressure_chart';
    pressureChart.width = 400;
    pressureChart.height = 300;
    $('#temp_container').append(tempChart);
    $('#hum_container').append(humChart);
    $('#wind_container').append(windChart);
    $('#pressure_container').append(pressureChart);
    var tempData = {
        labels: temp_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: temp_list
            }]
    };
    var humData = {
        labels: hum_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: hum_list
            }]
    }
    var windData = {
        labels: wind_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: wind_list
            }]
    }
    var pressureData = {
        labels: pressure_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: pressure_list
            }]
    }

    Chart.defaults.global.animationSteps = 50;
    Chart.defaults.global.tooltipYPadding = 16;
    Chart.defaults.global.tooltipCornerRadius = 0;
    Chart.defaults.global.tooltipTitleFontStyle = "normal";
    Chart.defaults.global.tooltipFillColor = "rgba(0,0,0,0.8)";
    Chart.defaults.global.animationEasing = "easeOutBounce";
    Chart.defaults.global.responsive = false;
    Chart.defaults.global.scaleLineColor = "black";
    Chart.defaults.global.scaleFontSize = 16;

    // get bar chart canvas
    var tempChart = document.getElementById("temp_chart").getContext("2d");
    var humChart = document.getElementById("hum_chart").getContext("2d");
    var windChart = document.getElementById("wind_chart").getContext("2d");
    var pressureChart = document.getElementById("pressure_chart").getContext("2d");

    steps = 10;
    var temp_max = 50;
    var hum_max = 60;
    var wind_max = 10;
    // draw bar chart
    new Chart(tempChart).Line(tempData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(temp_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,
        bezierCurve: false

    });

    new Chart(humChart).Bar(humData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(hum_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,
        bezierCurve: false
    });
    new Chart(windChart).Line(windData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(wind_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,

        bezierCurve: false
    });
    // new Chart(pressureChart).Line(pressureData, {
    //     scaleOverride: true,
    //     scaleSteps: steps,
    //     scaleStepWidth: Math.ceil(hum_max / steps),
    //     scaleStartValue: 0,
    //     scaleShowVerticalLines: false,
    //     scaleShowGridLines: false,
    //     barShowStroke: true,
    //     scaleShowLabels: true,
    //     bezierCurve: false
    // });
}
