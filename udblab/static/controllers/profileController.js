/**
 * Created by Nizomjon on 8/6/2017.
 */
app.controller('ProfileCtrl', ['$scope', '$log', '$http', '$location', '$sce', function ($scope, $log, $http, $location, $sce) {

    $log.info($location.path());
    $scope.user_load = function () {
        $http({
            url: '/api/v1/users/' + $('#user_id').val() + '/',
            method: 'GET',
            headers: {'Content-Type': 'application/json'},
        }).then(function (response) {
            $scope.user = response.data;
            $log.info($scope.user)
        })
    };
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}]);
