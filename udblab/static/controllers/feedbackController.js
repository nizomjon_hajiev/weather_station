/**
 * Created by Nizomjon on 8/6/2017.
 */
app.controller('FeedbackCtrl', ['$scope', '$log', '$http', '$location', function ($scope, $log, $http, $location) {
    $log.info($location.path());
    $scope.user_id = $('#user_id').val();
    $scope.location_id = $('#location_id').val();
    $scope.comment = '';

    // $log.info($scope.comment);
    //
    // var feedback = new Feedback($scope.user_id, $scope.location_id, '', $scope.comment);
    // $log.info(feedback.getGeneralInfo());
    $scope.send_feedback = function () {
        var feedback = new Feedback($scope.user_id, $scope.location_id, '', $scope.comment);
        $log.info(feedback.getGeneralInfo());
        $http({
            url: '/api/v1/feedback',
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            data: JSON.stringify(feedback.getGeneralInfo())
        }).then(function (response) {
            $log.info(response.data)
            $scope.comment = '';
        })
    };
    $scope.isActive = function (viewLocation) {
        $log.info(viewLocation === $location.path());
        $log.info(viewLocation);
        $log.info($location.path());
        return viewLocation === $location.path();
    };
}]);
