/**
 * Created by Nizomjon on 8/6/2017.
 */
app.controller('AnalysisCtrl', ['$scope', '$log', '$http', '$location', function ($scope, $log, $http, $location) {
    $log.info($location.path());
    $http({
        url: '/api/v1/analysis_test/',
        method: "GET",
        headers: {'Content-Type': 'application/json'},
    }).then(function (response) {
        $log.info(response.data.result);
        var json_data = response.data.result;
        $scope.hum = json_data.humidity.slice(0,4);
        $scope.temp = json_data.temp.slice(0,4);
        $scope.rain = json_data.rain.slice(0,4);
        $scope.wind_speed = json_data.ws.slice(0,4);
        $scope.dp = json_data.dp.slice(0,4);
        $scope.sf = json_data.sf.slice(0,4);
        $scope.max_sf = json_data.max_sf.slice(0,4);
        $scope.count_sf = json_data.count_sf.slice(0,4);
        $('#high_snow').html("&emsp; sm ( " + Math.round($scope.max_sf) + " )");
        $('#average_snow').html("&emsp; sm ( " + $scope.sf + " )");
        $('#snow_numbers').html($scope.count_sf);

    });

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}]);
