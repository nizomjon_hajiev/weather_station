/**
 * Created by Nizomjon on 8/6/2017.
 */

app.controller('MapCtrl', ['$scope', '$log', '$http', '$location', function ($scope, $log, $http, $location) {
    $scope.name = 'Map';
    $log.info($location.path());
    $scope.location_id = $('#location_id').val();
    $log.info($scope.location_id);
    $http.get('/api/v1/locations/' + $scope.location_id + '/')
        .then(function (success) {
            $log.info(success.data.sensor_nodes[0]);
            var sensor_node = success.data.sensor_nodes[0];
            $scope.latitude = sensor_node.latitude;
            $scope.longitude = sensor_node.longitude;
            initMap($scope.latitude, $scope.longitude)
        }, function (error) {
            $log.info(error)
        });
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}]);

function initMap(latitude, longitude) {
    var mapContainer = document.getElementById('map'), // 지도를 표시할 div
        mapOption = {
            center: new daum.maps.LatLng(latitude, longitude), // 지도의 중심좌표
            level: 3 // 지도의 확대 레벨
        };
    console.log("test");
    var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
    var marker = new daum.maps.Marker({
        position: map.getCenter()
    });

    marker.setMap(map);

}