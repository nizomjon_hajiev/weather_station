/**
 * Created by Nizomjon on 7/17/2017.
 */

var latitude = $('#latitude').val();
var longitude = $('#longitude').val();

if (latitude === '' && longitude === '') {
        $('#map').css('height','0px');
        $('#map_message').css('visibility','hidden');
    console.log('map0')
}

var mapContainer = document.getElementById('map'), // 지도를 표시할 div
    mapOption = {
        center: new daum.maps.LatLng(latitude, longitude), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };
console.log("test");
var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
var geocoder = new daum.maps.services.Geocoder();
var marker = new daum.maps.Marker({
    position: map.getCenter()
});

marker.setMap(map);
daum.maps.event.addListener(map, 'click', function (mouseEvent) {
    var latlng = mouseEvent.latLng;
    marker.setPosition(latlng);
    $('#latitude').val(latlng.getLat());
    $('#longitude').val(latlng.getLng());

});

$('#location_picker').locationpicker({
    location: {
        latitude: 46.1245,
        longitude: 2.27657
    },
    radius: 0,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        console.log("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")")
        $('#latitude').val(currentLocation.latitude);
        $('#longitude').val(currentLocation.longitude);
    }
});
$('#load').on('click', function () {

    document.getElementById("load").innerHTML="<i class='fa fa-spinner fa-spin'></i> 로딩";
    setTimeout(function () {
        document.getElementById("load").innerHTML="&emsp;발생&emsp;";
        $('#loader').removeClass('fa fa-spinner fa-spin');
        $('#sensor_node_unique_id').val(uuidv4());
    }, 4000);
});

function uuidv4() {
    var ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    var ID_LENGTH = 8;
    var rtn = '';
    for (var i = 0; i < ID_LENGTH; i++) {
        rtn += ALPHABET.charAt(Math.floor(Math.random() * ALPHABET.length));
    }
    return rtn;
}

function sample4_execDaumPostcode() {
    new daum.Postcode({
        oncomplete: function (data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
            var extraRoadAddr = ''; // 도로명 조합형 주소 변수

            // 법정동명이 있을 경우 추가한다. (법정리는 제외)
            // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
            if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                extraRoadAddr += data.bname;
            }
            // 건물명이 있고, 공동주택일 경우 추가한다.
            if (data.buildingName !== '' && data.apartment === 'Y') {
                extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }
            // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
            if (extraRoadAddr !== '') {
                extraRoadAddr = ' (' + extraRoadAddr + ')';
            }
            // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
            if (fullRoadAddr !== '') {
                fullRoadAddr += extraRoadAddr;
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById('sample4_postcode').value = data.zonecode; //5자리 새우편번호 사용
            document.getElementById('sample4_roadAddress').value = fullRoadAddr;
            document.getElementById('sample4_jibunAddress').value = data.jibunAddress;

            // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
            if (data.autoRoadAddress) {
                //예상되는 도로명 주소에 조합형 주소를 추가한다.
                var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
                document.getElementById('guide').innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

            } else if (data.autoJibunAddress) {
                var expJibunAddr = data.autoJibunAddress;
                document.getElementById('guide').innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

            } else {
                document.getElementById('guide').innerHTML = '';
            }
             $('#map').css('height','450px');
             $('#map_message').css('visibility','visible');
            geocoder.addr2coord(data.address, function (status, result) {
                // 정상적으로 검색이 완료됐으면
                if (status === daum.maps.services.Status.OK) {
                    // 해당 주소에 대한 좌표를 받아서
                    var coords = new daum.maps.LatLng(result.addr[0].lat, result.addr[0].lng);
                    // 지도를 보여준다.
                    mapContainer.style.display = "block";
                    map.relayout();
                    // 지도 중심을 변경한다.
                    map.setCenter(coords);
                    // 마커를 결과값으로 받은 위치로 옮긴다.
                    marker.setPosition(coords)
                    $('#latitude').val(result.addr[0].lat);
                    $('#longitude').val(result.addr[0].lng);
                }
            });
        }
    }).open();
}