/**
 * Created by Nizomjon on 6/9/2017.
 */
var sensor_node_id;
$(document).ready(function () {
    interceptMap();
    var start_time = getCurrentDate(true);
    var end_time = getCurrentDate(false);
    console.log('start_time: ' + start_time);
    console.log('end_time: ' + end_time);
    sensor_node_id = $('#sensor_node_id').val();
    $('#start_time').val(start_time);
    $('#end_time').val(end_time);
    searchBtn();
    // load_external_data();
    page_scroll();
});


function interceptMap() {
    $('#map').addClass('scrolloff'); // set the pointer events to none on doc ready
    $('#google_map').on('click', function () {
        $('#map').removeClass('scrolloff'); // set the pointer events true on click
    });

    $("#map").mouseleave(function () {
        $('#map').addClass('scrolloff'); // set the pointer events to none when mouse leaves the map area
    });
}

function getCurrentDate(isStartTime) {
    var today = new Date();
    var dd = today.getDate();
    if (isStartTime) {
        dd = today.getDate() - 10;
    }
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return yyyy + '-' + mm + '-' + dd
}

function load_analysed_data() {
    $.ajax({
        type: 'GET',
        url: '/api/v1/analysis_test/',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8'
    }).success(function (data) {
        var json_data = data['results']
        hum = json_data['']
    })
}

function load_sensor_node_data(id, i) {
    var data;
    var temperature_list = [];
    var temperature_list_date = [];
    var humidity_list = [];
    var humidity_list_date = [];
    var wind_speed_list = [];
    var wind_speed_list_date = [];
    var pressure_list = [];
    var pressure_list_date = [];
    sensor_node_id = id;
    var node_id = i + 1;
    data = {
        'sensor_node_id': id
    };
    $.ajax({
        url: '/api/v1/admin_sensor_node/',
        data: JSON.stringify(data),
        dataType: "json",
        type: 'POST',
        contentType: 'application/json;charset=UTF-8'
    }).success(function (data) {
        temperature_list = data['temperature_list'];
        humidity_list = data['humidity_list'];
        wind_speed_list = data['wind_speed_list'];
        pressure_list = data['pressure_list'];
        temperature_list_date = data['temperature_list_date'];
        humidity_list_date = data['humidity_list_date'];
        wind_speed_list_date = data['wind_speed_list_date'];
        pressure_list_date = data['pressure_list_date'];
        $('#node_id').html(node_id);
        drawGraph(temperature_list, humidity_list, wind_speed_list, pressure_list,
            temperature_list_date, humidity_list_date, wind_speed_list_date, pressure_list_date)

    })

}

function drawGraph(temp_list, hum_list, wind_list, pressure_list, temp_list_date, hum_list_date, wind_list_date, pressure_list_date) {
    // bar chart data
    $('#temp_chart').remove();
    $('#hum_chart').remove();
    $('#wind_chart').remove();
    $('#pressure_chart').remove();
    var tempChart = document.createElement('canvas');
    tempChart.id = 'temp_chart';
    tempChart.width = 400;
    tempChart.height = 300;
    var humChart = document.createElement('canvas');
    humChart.id = 'hum_chart';
    humChart.width = 400;
    humChart.height = 300;
    var windChart = document.createElement('canvas');
    windChart.id = 'wind_chart';
    windChart.width = 400;
    windChart.height = 300;
    var pressureChart = document.createElement('canvas');
    pressureChart.id = 'pressure_chart';
    pressureChart.width = 400;
    pressureChart.height = 300;
    $('#temp_container').append(tempChart);
    $('#hum_container').append(humChart);
    $('#wind_container').append(windChart);
    $('#pressure_container').append(pressureChart);
    var tempData = {
        labels: temp_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: temp_list
            }]
    };
    var humData = {
        labels: hum_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: hum_list
            }]
    }
    var windData = {
        labels: wind_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: wind_list
            }]
    }
    var pressureData = {
        labels: pressure_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: pressure_list
            }]
    }

    Chart.defaults.global.animationSteps = 50;
    Chart.defaults.global.tooltipYPadding = 16;
    Chart.defaults.global.tooltipCornerRadius = 0;
    Chart.defaults.global.tooltipTitleFontStyle = "normal";
    Chart.defaults.global.tooltipFillColor = "rgba(0,0,0,0.8)";
    Chart.defaults.global.animationEasing = "easeOutBounce";
    Chart.defaults.global.responsive = false;
    Chart.defaults.global.scaleLineColor = "black";
    Chart.defaults.global.scaleFontSize = 16;

    // get bar chart canvas
    var tempChart = document.getElementById("temp_chart").getContext("2d");
    var humChart = document.getElementById("hum_chart").getContext("2d");
    var windChart = document.getElementById("wind_chart").getContext("2d");
    var pressureChart = document.getElementById("pressure_chart").getContext("2d");

    steps = 10;
    var temp_max = 50;
    var hum_max = 60;
    var wind_max = 10;
    // draw bar chart
    new Chart(tempChart).Line(tempData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(temp_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,
        bezierCurve: false

    });

    new Chart(humChart).Bar(humData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(hum_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,
        bezierCurve: false
    });
    new Chart(windChart).Line(windData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(wind_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,

        bezierCurve: false
    });
    // new Chart(pressureChart).Line(pressureData, {
    //     scaleOverride: true,
    //     scaleSteps: steps,
    //     scaleStepWidth: Math.ceil(hum_max / steps),
    //     scaleStartValue: 0,
    //     scaleShowVerticalLines: false,
    //     scaleShowGridLines: false,
    //     barShowStroke: true,
    //     scaleShowLabels: true,
    //     bezierCurve: false
    // });
}

function searchBtn() {
    // var sensor_node_id = $('#sensor_node_id').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();
    var data;
    var temperature_list = [];
    var temperature_list_date = [];
    var humidity_list = [];
    var humidity_list_date = [];
    var wind_speed_list = [];
    var wind_speed_list_date = [];
    var pressure_list = [];
    var pressure_list_date = [];
    data = {
        'start_time': start_time,
        'end_time': end_time,
        'sensor_node_id': sensor_node_id
    };

    $.ajax({
        url: '/api/v1/filter_by_date/',
        dataType: 'json',
        data: JSON.stringify(data),
        type: 'POST',
        contentType: 'application/json;charset=UTF-8'
    }).success(function (data) {
        temperature_list = data['temperature_list'];
        humidity_list = data['humidity_list'];
        wind_speed_list = data['wind_speed_list'];
        pressure_list = data['pressure_list'];
        temperature_list_date = data['temperature_list_date'];
        humidity_list_date = data['humidity_list_date'];
        wind_speed_list_date = data['wind_speed_list_date'];
        pressure_list_date = data['pressure_list_date'];

        drawGraph(temperature_list, humidity_list, wind_speed_list, pressure_list,
            temperature_list_date, humidity_list_date, wind_speed_list_date, pressure_list_date)
    })


}
function load_history_table(id) {
    var data = {
        'id': id
    }
    $.ajax({
        url: '/api/v1/history/',
        data: JSON.stringify(data),
        dataType: 'json',
        type: 'POST',
        contentType: 'application/json;charset=UTF-8'
    }).success(function (data) {
        console.log(data);
        var history_list = data;
        var html = '';
        var j;
        if (history_list.length > 10) {
            j = 10;
        }
        else {
            j = history_list.length
        }
        for (var i = 0; i < j; i++) {
            var calc_time = Math.floor(history_list[i].calculated_time);
            var hour = 0;
            var minute = 0;
            var second = 0;
            var calculated_td_html = '';
            if (calc_time > 3600) {
                hour = parseInt(calc_time / 3600);
                minute = parseInt((calc_time % 3600) / 60);
                second = parseInt(calc_time % 60);
                calculated_td_html = '<td>' + hour + ' hour ' + minute + ' minute ' + second + ' second</td>'
            } else if ((calc_time < 3600) && (calc_time > 60)) {
                minute = parseInt((calc_time % 3600) / 60);
                second = parseInt(calc_time % 60);
                calculated_td_html = '<td>' + minute + ' minute ' + second + ' second</td>'
            } else {
                calculated_td_html = '<td>' + calc_time + ' second</td>'
            }
            var trHtml = '<tr align="CENTER">' +
                '<td>' + history_list[i].start_time + '</td>' +
                '<td>' + history_list[i].end_time + '</td>' +
                calculated_td_html +
                '</tr>'
            html += trHtml
        }
        $('#history_tbody').html(html);
    });


}

function feedback() {
    var comment = $('#comments').val()
    if (comment !== '') {
        var user_id = $('#user_id').val();
        var location_id = $('#user_id').val();
        var feedback = new Feedback(user_id, location_id, '', comment);
        $.ajax({
            url: '/api/v1/feedback',
            data: JSON.stringify(feedback.getGeneralInfo()),
            type: 'POST',
            dataType: "json",
            contentType: 'application/json;charset=UTF-8'

        }).success(function (response) {
            // alert('Success')
            // $("#fix .close").click();
            $('#comments').val('')

        });

    }
}

function load_last_seen_data(id) {
    $.ajax({
        url: '/api/v1/last_seen',
        data: JSON.stringify({'id': id}),
        type: 'POST',
        dataType: "json",
        contentType: 'application/json;charset=UTF-8'

    }).success(function (data) {
        var last_temp = data['last_temp'];
        var last_hum = data['last_hum'];
        var last_wind = data['last_wind'];
        var last_seen_date = data['created_on'];
        last_seen_date = last_seen_date.substring(0.20).split(' ');
        last_seen_date = last_seen_date[1] + ' ' + last_seen_date[2] + ' ' + last_seen_date[3] + ' ' + last_seen_date[4];
        $('#last_seen_container').html('');
        var html =
            '<h3><span>Last seen date: </span>' + last_seen_date + '</h3>' +
            '<div class="col-md-4">' +
            '<div id="hum" style="width:300px; height:280px"></div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div id="temp" style="width:300px; height:280px"></div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div id="wind_speed" style="width:300px; height:280px"></div>' +
            '</div>';
        $('#last_seen_container').html(html);
        var temp = new JustGage({
            id: "temp",
            value: last_hum,
            min: -10,
            max: 10,
            title: "Temperature"
        });
        var hum = new JustGage({
            id: "hum",
            value: last_temp,
            min: 0,
            max: 60,
            customSectors: {
                percents: true,
                ranges: [{
                    color: "#43bf58",
                    lo: 0,
                    hi: 50
                }, {
                    color: "#ff3b30",
                    lo: 51,
                    hi: 100
                }]
            },
            title: "Humidity %"
        });
        var wind_speed = new JustGage({
            id: "wind_speed",
            value: last_wind,
            min: 0,
            max: 10,
            title: "Wind Speed m/s"
        });
        console.log("New chart")
    }).fail(function (data) {
        console.log(data)
    })
}

function load_external_data() {
    $('.nav li').click(function (event) {
        $('.active').removeClass('active');
        $(this).addClass('active');
        $('#top').click(function () {
            $('.active').removeClass('active');
            $('#house').addClass('active');
        });
    });

    $.ajax({
        url: 'http://apis.skplanetx.com/weather/current/minutely?appKey=4ce0462a-3884-30ab-ab13-93efb1bc171f&version=1&lon=127.9259&lat=36.991',
        data: '',
        dataType: "json",
        type: 'GET',
        contentType: 'application/json;charset=UTF-8',

    }).success(function (data) {
        var weather = data['weather'];
        var minutly = weather['minutely'];
        var aa = minutly[0]
        var timeObservation = aa['timeObservation'];
        var station = aa['station'];
        var humidity = aa['humidity'];
        var wind = aa['wind'];
        var temperature = aa['temperature'];
        var pressure = aa['pressure'];
        var sky = aa['sky'];

        var city = station['name'];
        var wind_speed = wind['wspd'];
        var temperature = temperature['tc'];
        var pressure = pressure['surface'];
        var sky = sky['name'];

        $('.time').html(timeObservation);
        $('.current_city').html(city);
        $('.wind_speed').html(wind_speed);
        $('.temperature').html(temperature);
        $('.humidity').html(humidity);
        $('.pressure').html(pressure);
        $('.sky').html(sky);

    }).fail(function () {
        alert('error');
    });

}

function page_scroll() {
    // Add smooth scrolling to all links in navbar + footer link
    $(".navbar a, footer a[href='#myPage']").on('click', function (event) {

        // Prevent default anchor click behavior


        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 900, function () {

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
        });
    });

    // Slide in elements on scroll
    $(window).scroll(function () {
        $(".slideanim").each(function () {
            var pos = $(this).offset().top;

            var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
                $(this).addClass("slide");
            }
        });
    });
}