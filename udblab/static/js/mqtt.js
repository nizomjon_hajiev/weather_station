/**
 * Created by nizom on 1/29/17.
 */
var temp = 0.0;
var temperature = 0.0
var temperature1 = 0.0
var humidity = 0.0
var humidity1 = 0.0
var date = 0.0
var date1 = 0.0
const CURRENT_DATA_TOPIC = 'udblab/sensor/+/current_data/'
const FIXED_TIME_HUM_DATA_TOPIC = 'udblab/sensor/+/hum/fixed_time_data/'
const FIXED_TIME_TEMP_DATA_TOPIC = 'udblab/sensor/+/tem/fixed_time_data/'
const SENSOR_HEATING_DATA_TOPIC = 'udblab/sensor/heating/'
const CURRENT_DATA_SUFFIX = '/current_data/'
const FIXED_TIME_DATA_SUFFIX = '/fixed_time_data/'
const DURATION_OF_HEATING_DATA_TOPIC = 'udblab/sensor/+/duration_of_heating/'
const DURATION_OF_HEATING_DATA_SUFFIX = '/duration_of_heating/'
const PHOTO_DATA_SUFFIX = 'udblab/photo/'
const SENSORS_ID = [1111, 1112, 1113]
var client
var connected_broker = ''
function init_broker(broker_url, port, isMain, isHive, isSSL) {
    client = new Paho.MQTT.Client(broker_url, Number(port), '');
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    var connect_options = {
        timeout: 3,
        onSuccess: function () {
            console.log('Connected to ' + broker_url);
            client.subscribe(CURRENT_DATA_TOPIC, {qos: 0})
            client.subscribe(SENSOR_HEATING_DATA_TOPIC, {qos: 1})
            client.subscribe(FIXED_TIME_HUM_DATA_TOPIC, {qos: 1})
            client.subscribe(FIXED_TIME_TEMP_DATA_TOPIC, {qos: 2})
            client.subscribe(DURATION_OF_HEATING_DATA_TOPIC, {qos: 2})
        },
        onFailure: function (message) {
            if (isMain && isHive) {
                init_broker("m12.cloudmqtt.com", 33226, false, false, true)
            } else if (!isMain && !isHive) {
                init_broker("broker.hivemq.com", 8000, true, true, false)
            }
        },
        useSSL: !!isSSL,
        userName: isMain ? "udblab" : 'hlqbtvzv',
        password: isMain ? "12345" : 'rGA8NiRaD2KX',
    };

    client.connect(connect_options);
    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("Connection Lost:" + responseObject.errorMessage);
            init_broker("159.203.160.131", 8080, true, true)
        }
    }

    function onMessageArrived(message) {

        if (!String.prototype.endsWith) {
            String.prototype.endsWith = function (searchString, position) {
                var subjectString = this.toString();
                if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
                    position = subjectString.length;
                }
                position -= searchString.length;
                var lastIndex = subjectString.indexOf(searchString, position);
                return lastIndex !== -1 && lastIndex === position;
            };
        }

        var str = message.destinationName;
        var sensor_id = message.destinationName.slice(14, 18);
        if (str.endsWith(CURRENT_DATA_SUFFIX)) {
            if (sensor_id === String(SENSORS_ID[1])) {
                var json = message.payloadString
                var obj = JSON.parse(json);
                window.temperature = obj["temp"]
                window.humidity = obj["hum"]
                // $("#temperature").text(obj["temp"]);
                // $("#humidity").text(obj["hum"]);
                // $("#date").text(obj["date"]);
                console.log(sensor_id)
                console.log("Parsed json " + json);
                $('#temp_road').text("Temperature : " + obj["temp"] + " " + String.fromCharCode(176) + "C");
                $('#hum_road').text("Humidity : " + obj["hum"] + " %");
                $('#wind_road').text("Wind Speed : " + obj["wind_speed"] + " m/s");
                $('#date_road').text("Date : " + obj["date"]);
                $('#camera_background').css('background-image', 'url(' + 'data:image/png;base64,' + obj['image'] + '' + ')')

            } else if (sensor_id === String(SENSORS_ID[0])) {
                var json = message.payloadString
                var obj = JSON.parse(json);
                window.temperature1 = obj["temp"]
                window.humidity1 = obj["hum"]
                // $("#temperature1").text(obj["temp"]);
                // $("#humidity1").text(obj["hum"]);
                // $("#date1").text(obj["date"]);
                var image = new Image();
                image.src = 'data:image/png;base64,' + obj['image'] + '';
                document.body.appendChild(image);
                console.log(sensor_id)
                console.log("Parsed json " + json)
            }

        } else if (str.endsWith(FIXED_TIME_DATA_SUFFIX)) {
            console.log(message.destinationName)
            var json = message.payloadString
            console.log(json)
            $.ajax({
                url: '/api/v1/weather/',
                data: json,
                dataType: "json",
                type: 'POST',
                contentType: 'application/json;charset=UTF-8',
                success: function (response) {
                    console.log(response);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else if (str.endsWith(DURATION_OF_HEATING_DATA_SUFFIX)) {
            // console.log(message.destinationName)
            // var json = message.payloadString
            // console.log(json)
            // $.ajax({
            //     url: '/api/v1/heating_history/',
            //     data: json,
            //     dataType: "json",
            //     type: 'POST',
            //     contentType: 'application/json;charset=UTF-8',
            //     success: function (response) {
            //         console.log(response);
            //     },
            //     error: function (error) {
            //         console.log(error);
            //     }
            // });
        } else if (str.endsWith(PHOTO_DATA_SUFFIX)) {
            console.log(message.destinationName)
            var json = message.payloadString
            console.log(json)
            var image = new Image();
            image.src = 'data:image/png;base64,' + obj['image'] + '';
            document.body.appendChild(image);
        }

    }
}
init_broker("159.203.160.131", 8080, true, true, false)
