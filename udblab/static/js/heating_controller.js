/**
 * Created by Nizomjon on 5/29/2017.
 */
var interval;
var isRunning = false;
$(document).ready(function () {
    var myVar;
    $('#heating_controller_container button').click(function () {
        var time_value = $('#time_value').val().split(' ');
        console.log('Start timer');
        if ($(this).hasClass('locked')) {
            if (isRunning) {
                $('#offBtn').toggleClass(' btn-default btn-info');
                $('#onBtn').toggleClass(' btn-info btn-default');
                $('#switch_status').attr('class', 'alert alert-danger').html('Switched on.');
                clearTimeout(interval);
                $('#switch_status').attr('class', 'alert alert-info').html('Switched off.');
                $('#timer').remove();
                isRunning = false;

                const SENSOR_HEATING_DATA_TOPIC = 'udblab/sensor/1112/heating/'
                $("p").toggle();
                var message = new Paho.MQTT.Message("off");
                message.destinationName = SENSOR_HEATING_DATA_TOPIC;
                client.send(message);
                console.log('publishing new value:' + message);
            }

        } else if ($(this).hasClass('unlocked')) {
            if (!isRunning) {
                clearTimeout(interval);
                $('#offBtn').toggleClass('btn-info btn-default');
                $('#onBtn').toggleClass('btn-default btn-info');
                $('#switch_status').attr('class', 'alert alert-danger').html('Switched on.');
                var counter = time_value[0];
                $('#heating_container').append("<div id='timer' align='center' style='margin: 20px;'></div>");
                countdown("timer", counter, 0);

                const SENSOR_HEATING_DATA_TOPIC = 'udblab/sensor/1112/heating/'
                $("p").toggle();
                var message = new Paho.MQTT.Message("on");
                message.destinationName = SENSOR_HEATING_DATA_TOPIC;
                client.send(message);
                console.log('publishing new value:' + message);
            }
        }

    });


});

function countdown(elementName, minutes, seconds, timeout) {
    var element, endTime, hours, mins, msLeft, time;

    function twoDigits(n) {
        return (n <= 9 ? "0" + n : n);
    }

    function updateTimer() {
        msLeft = endTime - (+new Date);
        if (msLeft < 1000) {
            element.innerHTML = "countdown's over!";
            isRunning = false;
            clearTimeout(timeout)
        } else {
            time = new Date(msLeft);
            hours = time.getUTCHours();
            mins = time.getUTCMinutes();
            element.innerHTML = (hours ? hours + ':' + twoDigits(mins) : mins) + ':' + twoDigits(time.getUTCSeconds());
            console.log(hours ? hours + ':' + twoDigits(mins) : mins) + ':' + twoDigits(time.getUTCSeconds());
            isRunning = true;
            timeout = setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
            interval = timeout;

        }
    }

    element = document.getElementById(elementName);
    endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 1000;
    updateTimer();
}

