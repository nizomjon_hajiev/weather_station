/**
 * Created by Nizomjon on 24/07/2017.
 */
function show_map(latitude, longitude) {
    var container = document.getElementById('map');
    var options = {
        center: new daum.maps.LatLng(latitude, longitude),
        level: 3
    };

    var map = new daum.maps.Map(container, options);
    console.log(longitude)
    var marker = new daum.maps.Marker({
        position: map.getCenter()
    });

    marker.setMap(map);

    console.log("Map")
}