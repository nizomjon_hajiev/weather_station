/**
 * Created by Nizomjon on 5/2/2017.
 */
$(document).ready(function () {
    var locations = [];
    var sensor_nodes = [];
    var locations_sensors = [];
    var location_cor = [];
    var sensor_nodes_ids = []
    var url;
    var user_view_status = parseInt($('#user_view').val())
    if (user_view_status === 1) {
        url = '/api/v1/user_view/';
    } else if (user_view_status === 0) {
        url = '/api/v1/user/';
    }

    $.ajax({
        url: url,
        data: '',
        dataType: "json",
        type: 'GET',
        contentType: 'application/json;charset=UTF-8',
        success: function (response) {
            console.log(response);
            locations = response['locations']
            var test = locations[0]
            sensor_nodes = test["sensor_nodes"]


            for (var i = 0; i < sensor_nodes.length; i++) {
                var list = []
                list.push("sensor : " + i, sensor_nodes[i].latitude, sensor_nodes[i].longitude,
                    sensor_nodes[i].status, i, sensor_nodes[i].sensor_node_id);
                location_cor.push({lat: sensor_nodes[i].latitude, lng: sensor_nodes[i].longitude});
                sensor_nodes_ids.push(sensor_nodes[i].sensor_node_id)
                locations_sensors.push(list)
            }
            console.log(locations_sensors)

            var map = new google.maps.Map(document.getElementById('map'), {

                zoom: 17,
                center: new google.maps.LatLng(sensor_nodes[0].latitude, sensor_nodes[0].longitude),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;
            var marker_list = [];


            for (i = 0; i < locations_sensors.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations_sensors[i][1], locations_sensors[i][2]),
                    icon: icon_generator(locations_sensors[i][3]),
                    map: map,
                    animation: google.maps.Animation.DROP,

                });
                initInfoWindow(marker, 'Sensor node ' + (i + 1), map);


                marker_list.push(marker);
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        load_sensor_node_data(sensor_nodes_ids[i],i);
                        load_history_table(sensor_nodes_ids[i]);
                        load_last_seen_data(sensor_nodes_ids[i]);
                        clearAnimations(marker_list);
                        marker_list[i].setAnimation(google.maps.Animation.BOUNCE);
                        map.panTo(marker.getPosition());
                        // map.setCenter(marker.getPosition());
                    }
                })(marker, i));
            }
            var icons = ['http://maps.google.com/mapfiles/ms/icons/blue-dot.png', 'http://maps.google.com/mapfiles/ms/icons/red-dot.png']

            initMarkers(marker_list, sensor_nodes_ids)

            for (var j = 0; j <= 2; j++) {
                var div = document.createElement('div');
                div.innerHTML = '<img src="' + icons[j] + '"> ' + 'Active';
            }

            var flightPath = new google.maps.Polyline({
                path: location_cor,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });

            flightPath.setMap(map);
        },
        error: function (error) {
            alert(error)
            console.log(error);
        }
    });

});

function initMarkers(marker_list, sensor_nodes_ids) {
    marker_list[0].setAnimation(google.maps.Animation.BOUNCE);
    google.maps.event.trigger(marker_list[0], 'click');
    // load_history_table(sensor_nodes_ids[0]);
    // load_sensor_node_data(sensor_nodes_ids[0]);
}

function initInfoWindow(marker, message, map) {
    var infoWindow = new google.maps.InfoWindow({
        content: message
    })
    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
    });

}

function clearAnimations(marker_list) {
    for (var i = 0; i < marker_list.length; i++) {
        marker_list[i].setAnimation(null)
    }
}

function icon_generator(status) {
    if (status) {
        return 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    } else {
        return 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
    }
}

function sensor_fix_btn() {
    var sensor_node_id = $('#sensor_node_id').val();
    var location_id = $('#location_id').val();
    var user_id = $('#user_id').val();
    var message = $(".modal-body #feed_message").val();
    var feedback = new Feedback(user_id, location_id, sensor_node_id, message);
    $.ajax({
        url: '/api/v1/feedback',
        data: JSON.stringify(feedback.getInfo()),
        type: 'POST',
        dataType: "json",
        contentType: 'application/json;charset=UTF-8'

    }).success(function (response) {
        // alert('Success')
        $("#fix .close").click();


    });

}

function check_sensor_node_status(status, id, content, infowindow, map, marker) {
    if (status) {
        window.location = 'sensor_node?node=' + id;
        infowindow.setContent(content);
        infowindow.open(map, marker);
    } else {
        infowindow.setContent('<div >' +
            '<span style="margin-right: 10px; font-size: medium">Error</span>' +
            '<input id="sensor_node_id" value="' + id + '" type="hidden">' +
            '<button id="sensor_fix_btn" data-toggle="modal" data-target="#fix"  ' +
            'class="btn btn-primary" >Fix</button>' +
            '</div>');
        infowindow.open(map, marker);
    }
}

