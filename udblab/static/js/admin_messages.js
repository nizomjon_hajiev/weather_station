var app = angular.module('admin', []);
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}).controller('messageController', ['$scope', '$log', '$http', '$interval', '$location', function ($scope, $log, $http, $interval, $location) {


    $scope.user_location = [];
    $scope.feedback_list = [];
    $scope.get_list_feedback = function () {
        $http({
            url: '/api/v1/feedbacks/',
            method: 'GET'
        }).then(function (response) {
            $scope.list_feedback = response.data;
            // $log.info(response.data);
            angular.forEach($scope.list_feedback, function (feedback) {
                $http({
                    url: '/api/v1/users/' + feedback.user_id + '/',
                    method: 'GET'
                }).then(function (response) {
                    var value = {
                        'user': response.data.username,
                        'location': response.data.locations[0].road_address
                    };
                    // $log.info(value);

                    $scope.user_location.push(value);
                    $log.info('A' + $scope.user_location);


                })
            });
                $log.info('heyy');
            for (var i = 0; i < $scope.list_feedback.length; i++) {
                var status;
                if ($scope.list_feedback[i].response !== null) {
                    status = true
                } else {
                    status = false
                }
                var table_value = {
                    'address': $scope.user_location[i].location,
                    'user': $scope.user_location[i].user,
                    'status': status
                }
                $scope.feedback_list.push(table_value);
                $log.info($scope.feedback_list);
            }
        })
    }

}]);