/**
 * Created by Ahatajon on 15.05.2017.
 */
$(document).ready(function () {


    $('#add_location').click(function () {
        $('#location_container').append('<br><br><input type="text" placeholder="Location" id="n_location">' +
            '<input type="submit" id="ok" value="Add"><br>')
        console.log($('#ok').val())

        $('#ok').click(function () {
            var user_id = $('#user_id_value').val();
            var loc_address = $('#n_location').val();
            var data;
            data = {
                'type': 'location',
                'user_id': user_id,
                'address': loc_address
            }
            $.ajax({
                url: '/api/v1/attr_insert',
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data)

            }).success(function (result) {
                if (result.result !== 'error') {
                    var position = $("#locations > div").length;
                    console.log('position ' + position);
                    var location_id = result.result['location_id'];
                    console.log('location_id ' + location_id);
                    var location_address = result.result['location_address'];
                    console.log('location_address ' + location_address);
                    var html =
                        '<input id="exist_location_' + (position + 1) + '" value="' + location_id + '">' +
                        '<div id="location_' + (position + 1) + '" class="col-md-11 col-lg-offset-1">' +
                        '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">' +
                        '<div class="panel panel-default">' +
                        '<div class="panel-heading" role="tab" id="heading_' + (position + 1) + '">' +
                        '<h4 class="panel-title">' +
                        '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_' + (position + 1) + '"' +
                        'aria-expanded="true" aria-controls="collapse_' + (position + 1) + '" >' + location_address + '</a>' +

                        '<button style="float: right" data-toggle="modal" title="Add this item" data-location_id="' + location_id + '"' +
                        'data-location_position="' + (position + 1) + '" class="open-AddBookDialog btn btn-danger btn-xs" href="#addBookDialog"' +
                        '>' +
                        '<span class="glyphicon glyphicon-trash"></span>' +
                        '</button>' +
                        '</h4>' +
                        '</div>' +
                        '<div id="collapse_' + (position + 1) + '" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne">' +
                        '<div class="panel-body">' +
                        '<div id="form-inline" class="form-inline">' +
                        '<div id="made_sensors_loc_' + (position + 1) + '"></div>' +
                        '<br>' +
                        '<div id="add_sensor_form">' +
                        '<div class="form-group">' +
                        '<label for="sensor_latitude">Latitude</label><input id="sensor_latitude_loc_' + (position + 1) + '" type="text" class="form-control" placeholder="Latitude">' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<label for="sensor_latitude">Longitude</label><input id="sensor_longitude_loc_' + (position + 1) + '" type="text" class="form-control" placeholder="Longitude">' +
                        '</div>' +
                        // '<div class="form-group">' +
                        // '<label for="sensor_type">Type</label>' +
                        // '<select id="sensor_type_loc_' + (position + 1) + '" class="form-control">' +
                        // '<option>temperature</option>' +
                        // '<option>humidity</option>' +
                        // '<option>wind_speed</option>' +
                        // '</select>' +
                        // '</div>' +
                        '<button id="add_sensor_loc_' + (position + 1) + '" type="button" class="btn btn-primary" onclick="addSensorNode(' + (position + 1) + ')" value="' + location_id + '">Add Sensor</button>' +
                        '</div>' +
                        '<br>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                    $('#locations').append(html);
                    //TODO adding delete button with dynamic values
                    console.log($('#add_sensor').val())


                }
            }).fail(function () {

            })
        });

    });
    $('#add_sensor').click(function () {
        var sensor_lat = $('#sensor_latitude').val()
        var sensor_long = $('#sensor_longitude').val()
        var sensor_type = $('#sensor_type').val()
        var location_id = $('#add_sensor').val()
        var data;
        if ((sensor_lat.length > 0) && (sensor_long.length > 0)) {

            data = {
                "location_id": location_id,
                "sensor_latitude": sensor_lat,
                "sensor_longitude": sensor_long,
                "sensor_type": sensor_type,
                'type': 'sensor'
            };
            $.ajax({
                url: '/api/v1/attr_insert',
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data)

            }).success(function (result) {
                if (result.result !== 'error') {
                    manage_message(true)
                    var i = $("#made_sensors > div").length
                    var latitude = result.result['latitude'];
                    var longitude = result.result['longitude'];
                    var sensor_type = result.result['sensor_type'];
                    var html = '<div id="sensor_node_' + (i + 1) + '" class="for-sensors">' +
                        '<h4>Sensor: <span>' + (i + 1) + '</span></h4>' +
                        '<div class="form-group">' +
                        '<label for="exampleInputLatitude">Latitude</label>' +
                        '<input value="' + latitude + '" type="text" class="form-control" id="exampleInputLatitude" placeholder="Latitude" disabled>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<label for="exampleInputLatitude">Longitude</label>' +
                        '<input value="' + longitude + '" type="text" class="form-control" id="exampleInputLongitude" placeholder="Longitude" disabled>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<label for="exampleInputType">Sensor Type</label>' +
                        '<input value="' + sensor_type + '" type="text" class="form-control" id="exampleInputType" placeholder="Sensor Type" disabled>' +
                        '</div>' +
                        '<br>' +
                        '</div>';
                    $('#made_sensors').append(html)
                    $('#sensor_latitude').val("")
                    $('#sensor_longitude').val("")
                } else {
                    manage_message(false)
                }
            }).fail(function () {
                manage_message(false)
            })

        } else {
            manage_message(false)
            // alert('You have to enter all values!')
        }
    });


    function manage_message(isSuccess) {
        if (isSuccess) {


        } else {

        }
    }


});

function addSensorNode(div_position) {
    var sensor_lat = $('#sensor_latitude_loc_' + div_position).val()
    var sensor_long = $('#sensor_longitude_loc_' + div_position).val()
    var sensor_type = $('#sensor_type_loc_' + div_position).val()
    var location_id = $('#add_sensor_loc_' + div_position).val()
    var data;
    if ((sensor_lat.length > 0) && (sensor_long.length > 0)) {

        data = {
            "location_id": location_id,
            "sensor_latitude": sensor_lat,
            "sensor_longitude": sensor_long,
            "sensor_type": sensor_type,
            'type': 'sensor_node'
        };
        $.ajax({
            url: '/api/v1/attr_insert',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data)

        }).success(function (result) {
            if (result.result !== 'error') {
                // manage_message(true)
                var s = "#made_sensors_loc_" + div_position;
                var i = $(s).find("> div").length;
                var latitude = result.result['latitude'];
                var longitude = result.result['longitude'];
                var sensor_type = result.result['sensor_type'];
                var sensor_node_id = result.result['sensor_node_id'];
                var html = '<div id="sensor_node_' + (i + 1) + '" class="for-sensors">' +
                    '<h4>Sensor: <span>' + (i + 1) + '</span></h4>' +
                    '<label for="exampleInputLatitude">Latitude</label>' +
                    '<input value="' + latitude + '" type="text" class="form-control" id="exampleInputLatitude" placeholder="Latitude" disabled>' +
                    '<label for="exampleInputLatitude">Longitude</label>' +
                    '<input value="' + longitude + '" type="text" class="form-control" id="exampleInputLongitude" placeholder="Longitude" disabled>' +
                    '<button data-toggle="modal" data-sensor_id="' + sensor_node_id + '"></button>' +
                    // '<label for="exampleInputType">Type</label>' +
                    // '<input value="' + sensor_type + '" type="text" class="form-control" id="exampleInputType" placeholder="Sensor Type" disabled>' +
                    '</div>'
                ;
                $(s).append(html)
                var lat_pos = "#sensor_latitude_loc_" + div_position;
                var lon_pos = "#sensor_longitude_loc_" + div_position;
                var j = $(s).find("> div").length;
                $(lat_pos).val("");
                $(lon_pos).val("");
            } else {
                manage_message(false)
            }
        }).fail(function () {
            manage_message(false)
        })

    } else {
        manage_message(false)
        // alert('You have to enter all values!')
    }

}

$(document).on("click", ".open-AddBookDialog", function () {
    var sensorId = $(this).data('sensor_id');
    var locationId = $(this).data('location_id');
    var locationPos = $(this).data('location_position');
    var sensorPos = $(this).data('sensor_position');
    $(".modal-body #sensorId").val(sensorId);
    $(".modal-body #locationId").val(locationId);
    $(".modal-body #locationPos").val(locationPos);
    $(".modal-body #sensorPos").val(sensorPos);
    // As pointed out in comments,
    // it is superfluous to have to manually call the modal.
    $('#addBookDialog').modal('show');
});
function deleteSensor() {
    var sensorId = $('#sensorId').val();
    var locationId = $('#locationId').val();
    var locationPos = $('#locationPos').val();
    var sensorPos = $('#sensorPos').val();
    var userId = $('#user_id_value').val();


    var type;

    if (sensorId.length <= 0 || sensorPos.length <= 0) {
        type = 'location'
    } else {
        type = 'sensor'
    }


    var data;
    data = {
        'type': type,
        'sensor_id': sensorId,
        'location_id': locationId,
        'user_id': userId
    };
    $.ajax({
        url: '/api/v1/attr_insert',
        type: 'DELETE',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data)

    }).success(function (result) {
        if (result.result !== 'error') {
            if (type === 'sensor')
                $('#location_' + locationPos).find('#sensor_node_' + sensorPos).html('');
            else if (type === 'location') {
                $('#location_' + locationPos).html('');
            }
            $('.modal').modal('hide');
        }

    }).fail(function () {

    })
}