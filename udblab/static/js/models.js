/**
 * Created by Nizomjon on 6/2/2017.
 */
function Feedback(user_id, location_id, sensor_node_id, message) {
    this.user_id = user_id;
    this.location_id = location_id;
    this.sensor_node_id = sensor_node_id;
    this.message = message;

    this.getInfo = function () {
        return {
            'user_id': this.user_id,
            'location_id': this.location_id,
            'sensor_node_id': this.sensor_node_id,
            'message': this.message,
            'type': 'user'
        }
    }

    this.getGeneralInfo = function () {
        return {
            'user_id': this.user_id,
            'message': this.message,
            'location_id': this.location_id,
            'type': 'general'
        }
    }
}
