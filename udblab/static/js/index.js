/**
 * Created by Nizomjon on 5/2/2017.
 */
$(document).ready(function () {
    var url_dashboard = 'dashboard';
    var url_users = 'users';
    var url_locations = 'locations';
    var url_view='view';
    var url_details='details';
    var url_analysis = 'analysis';

    var url_path = window.location.pathname;
    var a = url_path.split('_')
    switch (a[1]) {
        case url_dashboard:
            $('#side_dashboard').addClass("selected");
            break;
        case url_users:
            $('#side_users').addClass("selected");
            break;
        case url_locations:
            $('#side_locations').addClass("selected");
            break;
        case url_view:
            $('#side_view').addClass("selected");
            break;
        case url_details:
            $('#side_details').addClass("selected");
            break;
        case url_analysis:
            $('#side_analysis').addClass("selected");
            break;

    }

    $(document).on("click", ".reply", function () {
        var feed_id = $(this).data('feed_id');
        var feed_email = $(this).data('feed_email');
        var feed_owner = $(this).data('feed_owner');
        $(".modal-body #feedId").val(feed_id);
        $(".modal-body #feedEmail").val(feed_email);
        $(".modal-body #feedOwner").val(feed_owner);
        $('#reply').modal('show');
    });

    $('#replyBtn').click(function () {
        var feed_id = $('#feedId').val();
        var feed_email = $('#feedEmail').val();
        var feed_owner = $('#feedOwner').val();
        var feed_response = $('#feed_response').val();
        var data;
        data = {
            'feed_id': feed_id,
            'feed_email': feed_email,
            'feed_owner': feed_owner,
            'feed_response': feed_response
        }
        if (feed_response.length > 0) {
            $.ajax({
                url: '/api/v1/feedback',
                type: 'POST',
                data: JSON.stringify(data)

            }).success(function (result) {
                if (result.result !== 'error') {

                }

            }).fail(function () {

            })
        } else {
            alert("Please fill the required fields")
        }

    })
});