/**
 * Created by Nizomjon on 5/28/2017.
 */
// Morris.js Charts sample data for SB Admin template

$(document).ready(function () {


    var location_id = $('#location_id').val();
    var data;
    var temperature_list = [];
    var temperature_list_date = [];
    var humidity_list = [];
    var humidity_list_date = [];
    data = {
        'location_id': location_id
    };

    $.ajax({
        url: '/api/v1/admin_location_map/',
        data: JSON.stringify(data),
        dataType: "json",
        type: 'POST',
        contentType: 'application/json;charset=UTF-8'
    }).success(function (data) {
        temperature_list = data['temperature_list'];
        humidity_list = data['humidity_list'];
        temperature_list_date = data['temperature_list_date'];
        humidity_list_date = data['humidity_list_date'];

        // bar chart data
        var tempData = {
            labels: temperature_list_date,
            datasets: [
                {
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    bezierCurve: false,
                    data: temperature_list
                }]
        };
        var humData = {
            labels: humidity_list_date,
            datasets: [
                {
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    bezierCurve: false,
                    data: humidity_list
                }]
        }

        Chart.defaults.global.animationSteps = 50;
        Chart.defaults.global.tooltipYPadding = 16;
        Chart.defaults.global.tooltipCornerRadius = 0;
        Chart.defaults.global.tooltipTitleFontStyle = "normal";
        Chart.defaults.global.tooltipFillColor = "rgba(0,0,0,0.8)";
        Chart.defaults.global.animationEasing = "easeOutBounce";
        Chart.defaults.global.responsive = false;
        Chart.defaults.global.scaleLineColor = "black";
        Chart.defaults.global.scaleFontSize = 16;

        // get bar chart canvas
        var tempChart = document.getElementById("temp_chart").getContext("2d");
        var humChart = document.getElementById("hum_chart").getContext("2d");

        steps = 10
        var temp_max = 100;
        var hum_max = 100;
        // draw bar chart
        new Chart(tempChart).Line(tempData, {
            scaleOverride: true,
            scaleSteps: steps,
            scaleStepWidth: Math.ceil(temp_max / steps),
            scaleStartValue: 0,
            scaleShowVerticalLines: false,
            scaleShowGridLines: false,
            barShowStroke: true,
            scaleShowLabels: true,
            bezierCurve: false

        });

        new Chart(humChart).Line(humData, {
            scaleOverride: true,
            scaleSteps: steps,
            scaleStepWidth: Math.ceil(hum_max / steps),
            scaleStartValue: 0,
            scaleShowVerticalLines: false,
            scaleShowGridLines: false,
            barShowStroke: true,
            scaleShowLabels: true,
            bezierCurve: false
        })
    })

});

