$(document).ready(function () {

    var sensor_node_id = $('#sensor_node_id').val();
    var update_sensor_data = function () {
        $.ajax({
            url: '/api/v1/sensor_models/' + sensor_node_id + '/',
            type: 'GET',
            contentType: "application/json; charset=utf-8",
        }).success(function (response) {
            console.log(response);
            console.log(response.data);
            $('#temp').html(response.temperature)
            $('#hum').html(response.humidity)
            $('#wind').html(response.wind_speed)
            $('#date').html(response.created_on)

        }).fail(function () {
            manage_message(false)
        })
    };

    setInterval(update_sensor_data,5000)


});