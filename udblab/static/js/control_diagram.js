/**
 * Created by Ahatajon on 25.07.2017.
 */
$( document ).ready(function () {

});
var MC_1=0;
var MC_2=0;
var MC_3=0;
var color_style_1;
var color_style_2;
var color_style_3;

      if(MC_1==0){
            color_style_1="red";
      } else{
          color_style_1 = "green";
      }
      if(MC_2 == 0) {
          color_style_2="red";
      }else if(MC_2 == 1){
           color_style_2="green";
      }
      if(MC_3 == 0){
            color_style_3="red";
      }else if(MC_3 == 1){
            color_style_3="green";
      }

function control_panel(mc_1_status,mc_2_status,mc_3_status) {
      var c = document.getElementById("myControl");
      var mc=c.getContext("2d");
      var ms1 = c.getContext("2d");
      var ms2=c.getContext("2d");
      var ms3=c.getContext("2d");
      var ms4=c.getContext("2d");

// min.beginPath()
//       min.font="16px Arial ";
//       min.fillStyle='black';
//       min.fillText("Min (-12 C) ",152,170);
//       min.stroke();

      mc.beginPath();
      mc.font="16px Arial black";
      mc.fillStyle="black";
      mc.fillText("Control panel",0, 15);
      mc.fillText("C_P",175, 30);
      mc.arc(150, 20, 15, 0,2*Math.PI);
      mc.fillStyle = 'red';
      mc.moveTo(150,35);
      mc.lineTo(150,45);
      mc.fill();
      mc.stroke();

      ms1.beginPath();
      ms1.font="14px Arial black";
      ms1.fillStyle="black";
      ms1.fillText("MC_1 ",40, 105);
      ms1.arc(50,75,15,0,2*Math.PI);
      ms1.fillStyle = color_style_1;
      ms1.fill();
      ms1.moveTo(50,60);
      ms1.lineTo(50,45);
      ms1.stroke();

      ms2.beginPath();
      ms2.font="14px Arial black";
      ms2.fillStyle="black";
      ms2.fillText("MC_2 ",140, 105);
      ms2.arc(150, 75,15, 0,2*Math.PI);
      ms2.fillStyle = color_style_2;
      ms2.fill();
      ms2.moveTo(150,60);
      ms2.lineTo(150,45);
      ms2.stroke();

      ms3.beginPath();
      ms3.font="14px Arial black";
      ms3.fillStyle="black";
      ms3.fillText("MC_3 ",240, 105);
      ms3.arc(250, 75,15, 0,2*Math.PI);
      ms3.fillStyle = color_style_3;
      ms3.fill();
      ms3.moveTo(250,60);
      ms3.lineTo(250,45);
      ms3.stroke();

      ms4.beginPath();
      ms4.moveTo(50,45);
      ms4.lineTo(250,45);
      ms4.stroke();
}