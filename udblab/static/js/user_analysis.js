/**
 * Created by Ahatajon on 27.05.2017.
 */
console.log('Analysis');


function doFirst(temp) {
    var canvas = document.getElementById('myCanvas');
    var context = canvas.getContext('2d');
    var context2 = canvas.getContext('2d');
    var number = canvas.getContext('2d');
    var centerX = canvas.width / 2;
    var centerY = canvas.height / 2;
    var radius = 50;
    var radius2 = Math.round(temp);
    context.beginPath();
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'lightblue';
    context.fill();
    context.lineWidth = 0;
    context.strokeStyle = '#00aaff';
    context.stroke();

    context2.beginPath();
    context2.arc(centerX, centerY, radius, 0, 2 * (Math.PI * (radius2 / 100)), false);
    context2.fillStyle = 'lightblue';
    context2.fill();
    context2.lineWidth = 15;
    context2.strokeStyle = '#00aaff';
    context2.stroke();

    // number uchun yozuvi uchun
    number.beginPath()
    number.font = "32px Arial ";
    number.fillStyle = 'white';
    number.fillText(radius2, centerX - 15, centerY + 10);
    number.stroke();
}


function doSecond(hum) {
    var canvas = document.getElementById('myCanvas1');
    var context = canvas.getContext('2d');
    var context2 = canvas.getContext('2d');
    var number = canvas.getContext('2d');
    var centerX = canvas.width / 2;
    var centerY = canvas.height / 2;
    var radius = 50;
    var radius2 = Math.round(hum);

    context.beginPath();
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'lightblue';
    context.fill();
    context.lineWidth = 0;
    context.strokeStyle = '#00aaff';
    context.stroke();

    context2.beginPath();
    context2.arc(centerX, centerY, radius, 0, 2 * (Math.PI * (radius2 / 100)), false);
    context2.fillStyle = 'lightblue';
    context2.fill();
    context2.lineWidth = 15;
    context2.strokeStyle = '#00aaff';
    context2.stroke();
    // number uchun yozuvi uchun
    number.beginPath()
    number.font = "32px Arial ";
    number.fillStyle = 'white';
    number.fillText(radius2, centerX - 15, centerY + 10);
    number.stroke();
}

function doThird(wind_speed) {
    var canvas = document.getElementById('myCanvas2');
    var context = canvas.getContext('2d');
    var context2 = canvas.getContext('2d');
    var number = canvas.getContext('2d');
    var centerX = canvas.width / 2;
    var centerY = canvas.height / 2;
    var radius = 50;
    var radius2 = Math.round(wind_speed);

    context.beginPath();
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'lightblue';
    context.fill();
    context.lineWidth = 0;
    context.strokeStyle = '#00aaff';
    context.stroke();

    context2.beginPath();
    context2.arc(centerX, centerY, radius, 0, 2 * (Math.PI * (radius2 / 100)), false);
    context2.fillStyle = 'lightblue';
    context2.fill();
    context2.lineWidth = 15;
    context2.strokeStyle = '#00aaff';
    context2.stroke();
    // number uchun yozuvi uchun
    number.beginPath()
    number.font = "32px Arial ";
    number.fillStyle = 'white';
    number.fillText(radius2, centerX - 15, centerY + 10);
    number.stroke();
}

function doFourth(dp) {
    var canvas = document.getElementById('myCanvas3');
    var context = canvas.getContext('2d');
    var context2 = canvas.getContext('2d');
    var number = canvas.getContext('2d');
    var centerX = canvas.width / 2;
    var centerY = canvas.height / 2;
    var radius = 50;
    var radius2 = Math.round(dp);

    context.beginPath();
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'lightblue';
    context.fill();
    context.lineWidth = 0;
    context.strokeStyle = '#00aaff';
    context.stroke();

    context2.beginPath();
    context2.arc(centerX, centerY, radius, 0, 2 * (Math.PI * (radius2 / 100)), false);
    context2.fillStyle = 'lightblue';
    context2.fill();
    context2.lineWidth = 15;
    context2.strokeStyle = '#00aaff';
    context2.stroke();
    // number uchun yozuvi uchun
    number.beginPath()
    number.font = "32px Arial ";
    number.fillStyle = 'white';
    number.fillText(radius2, centerX - 15, centerY + 10);
    number.stroke();
}


// window.addEventListener("load", doFourth, false);
//
// function doFifth(radius) {
//     var canvas = document.getElementById('myCanvas4');
//     var context = canvas.getContext('2d');
//     var min = canvas.getContext('2d');
//     var max = canvas.getContext('2d');
//     var avgmax = canvas.getContext('2d');
//     var avgmin = canvas.getContext('2d');
//
//
//     var centerX = canvas.width / 2;
//     var centerY = canvas.height / 2;
//     var lineColor = 'rgb(0, 137, 255)';
//
//     context.beginPath();
//     context.moveTo(centerX, centerY);
//     context.lineTo(centerX, 20);
//     context.lineWidth = 15;
//     context.strokeStyle = lineColor;
//     context.lineCap = 'round';
//     context.stroke();
//
//     context.beginPath();
//     context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
//     context.fillStyle = 'white';
//     context.lineWidth = 5;
//     context.fill();
//     context.strokeStyle = lineColor;
//     context.stroke();
//
//     context.beginPath();
//     context.moveTo(centerX, centerY);
//     context.lineTo(centerX, 20);
//     context.lineWidth = 5;
//     context.strokeStyle = 'rgb(255, 255, 255)';
//     context.lineCap = 'round';
//     context.stroke();
//
//     context.beginPath();
//     context.moveTo(135, 40);
//     context.lineTo(150, 40);
//     context.lineWidth = 3;
//     context.strokeStyle = lineColor;
//     context.lineCap = 'round';
//     context.stroke();
//
//     context.beginPath();
//     context.moveTo(135, 170);
//     context.lineTo(150, 170);
//     context.lineWidth = 3;
//     context.strokeStyle = lineColor;
//     context.lineCap = 'round';
//     context.stroke();
//
//     context.beginPath();
//     context.moveTo(115, 50);
//     context.lineTo(100, 50);
//     context.lineWidth = 3;
//     context.strokeStyle = lineColor;
//     context.lineCap = 'round';
//     context.stroke();
//
//     context.beginPath();
//     context.moveTo(115, 160);
//     context.lineTo(100, 160);
//     context.lineWidth = 3;
//     context.strokeStyle = lineColor;
//     context.lineCap = 'round';
//     context.stroke();
//     // Max yozuvi uchun
//     max.beginPath()
//     max.font = "16px Arial ";
//     max.fillStyle = 'black';
//     max.fillText("Max (43 C) ", 152, 39);
//     max.stroke();
//     // Min yozuvi uchun
//     min.beginPath()
//     min.font = "16px Arial ";
//     min.fillStyle = 'black';
//     min.fillText("Min (-12 C) ", 152, 170);
//     min.stroke();
//     // Avgmax yozuvi uchun
//     avgmax.beginPath()
//     avgmax.font = "16px Arial ";
//     avgmax.fillStyle = 'black';
//     avgmax.fillText("Avg max", 30, 48);
//     avgmax.fillText("(32 C)", 35, 65);
//     avgmax.stroke();
//     // Avgmin yozuvi uchun
//     avgmin.beginPath()
//     avgmin.font = "16px Arial ";
//     avgmin.fillStyle = 'black';
//     avgmin.fillText("Avg min", 30, 158);
//     avgmin.fillText("(-6 C) ", 35, 175);
//     avgmin.stroke();
//
// }
// window.addEventListener("load", doFifth, false);