var app = angular.module("app", ["ngRoute"]);

app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "static/partials/main.html",
            controller: "MainCtrl"
        })
        .when("/map", {
            templateUrl: "static/partials/map.html",
            controller: "MapCtrl"
        })
        .when('/details', {
            templateUrl: "static/partials/details.html",
            controller: "DetailCtrl"
        })
        .when('/analysis', {
            templateUrl: "static/partials/analysis.html",
            controller: "AnalysisCtrl"
        })
        .when('/feedback', {
            templateUrl: "static/partials/feedback.html",
            controller: "FeedbackCtrl"
        })
        .when('/profile', {
            templateUrl: "static/partials/profile.html",
            controller: "ProfileCtrl"
        });
}).config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});



