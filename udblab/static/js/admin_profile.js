/**
 * Created by Nizomjon on 5/13/2017.
 */

function editBtn() {

        var user_id=$('#user_id').val();
    var html =
        '<div id="password_reset_form" style=" margin: 25px 0 0 0;">' +
        '<input id="user_id" value="'+user_id+'" type="hidden">' +
        '<label for="current_password">Current Password</label>' +
        '<input id="current_password" name="current_password" class="form-control"' +
        'type="password" placeholder="Current Password">' +
        '<label for="new_password">New Password</label>' +
        '<input id="new_password" name="new_password" class="form-control"' +
        'type="password" placeholder="New Password">' +

        '<label for="confirm_password">Confirm Password</label>' +
        '<input id="confirm_password" name="confirm_password" class="form-control"' +
        'type="password" placeholder="Confirm Password">' +

        '<div class="btn-group" style="margin-top: 10px">' +
        '<button id="cancelBtn" onclick="cancelBtn()" type="button" style="margin: 10px" class="btn btn-default">' +
        'Cancel </button>' +
        '<button id="updateBtn" onclick="updateBtn()" type="button" style="margin: 10px" class="btn btn-primary">' +
        'Update</button>' +
        '</div>' +

        '<div id="waring_message" class="alert alert-warning" style="visibility: hidden">' +
        '<strong>Warning!</strong> Please fill fields correctly.' +
        '</div>' +
        '</div>';
    $('#profile').append(html);
    $('#editBtn').remove();
    $(this).toggle(false);
    $('#profile').find('#success_message').remove();
    $('#profile').find('#warning_message').remove();
    $('#waring_message').css('display', 'none');
    $('#success_message').css('display', 'none');

}

function cancelBtn() {

        var user_id = $('#user_id');
    $('#password_reset_form').remove();
    var html1 =
        '<input id="user_id" value='+user_id+' type="hidden">'+
        '<button style="margin: 25px 0 10px 20px;" id="editBtn" onclick="editBtn()" type="button" class="btn btn-primary">'+
        'Edit' +
        '</button>';
    $('#profile').append(html1);
    // $('#editBtn').toggle(true);
    $('#waring_message').css('visibility', 'hidden');
    $('#success_message').css('visibility', 'hidden');
}

function updateBtn() {
    var current_pwd = $('#current_password').val();
    var new_pwd = $('#new_password').val();
    var confirm_pwd = $('#confirm_password').val();
    var user_id = $('#user_id').val();
    var data;
    if (current_pwd.length !== 0 || new_pwd.length !== 0 || confirm_pwd.length !== 0) {
        if (new_pwd === confirm_pwd) {
            data = {
                "user_id": user_id,
                "current_password": current_pwd,
                "new_password": new_pwd,
                "confirm_password": confirm_pwd
            };
            $.ajax({
                url: '/api/v1/password_reset',
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data)
            }).success(function (result) {
                // alert(result.result)
                if (result.result === 'success') {
                    manage_message(true)
                } else {
                    manage_message(false)
                }
            }).fail(function () {
                manage_message(false)
            })
        } else {
            manage_message(false)
        }
    } else {
        manage_message(false)
    }
}

function manage_message(isSuccess) {
    if (isSuccess) {
            var user_id = $('#user_id');
        $('#password_reset_form').remove();
        var html2 =
            '<input id="user_id" value='+user_id+' type="hidden">'+
            '<button style="margin: 25px 0 10px 20px;" id="editBtn" onclick="editBtn()" type="button" class="btn btn-primary">'+
            'Edit' +
            '</button>'+
            '<div id="success_message" class="alert alert-success">' +
            '<strong>Success!</strong> Successfully changed'+
            '</div>';
        $('#profile').append(html2);

        $('#success_message').fadeOut(2500)
    } else {
         $('#profile').find('#warning_message').remove();
        $('#editBtn').toggle(true);
        $('#profile').append('<div id="warning_message" class="alert alert-warning">' +
        '<strong>Error!</strong> Fullfill fields correctly' +
        '</div>');
        $('#warning_message').fadeOut(2000);
    }
}