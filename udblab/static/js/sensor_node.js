/**
 * Created by Nizomjon on 5/28/2017.
 */
// Morris.js Charts sample data for SB Admin template

$(document).ready(function () {

    var last_temp = $('#last_temperature').val();
    var last_hum = $('#last_humidity').val();


    var temp = new JustGage({
        id: "temp",
        value: last_temp,
        min: 0,
        max: 100,
        title: "Temperature"
    });
    var hum = new JustGage({
        id: "hum",
        value: last_hum,
        min: 0,
        max: 100,
        title: "Humidity"
    });
    var pressure = new JustGage({
        id: "pressure",
        value: 67,
        min: 0,
        max: 100,
        title: "Pressure"
    });

    init();

});

function init() {
    var sensor_node_id = $('#sensor_node_id').val();
    var data;
    var temperature_list = [];
    var temperature_list_date = [];
    var humidity_list = [];
    var humidity_list_date = [];
    var wind_speed_list = [];
    var wind_speed_list_date = [];
    var pressure_list = [];
    var pressure_list_date = [];
    data = {
        'sensor_node_id': sensor_node_id
    };

    $.ajax({
        url: '/api/v1/admin_sensor_node/',
        data: JSON.stringify(data),
        dataType: "json",
        type: 'POST',
        contentType: 'application/json;charset=UTF-8'
    }).success(function (data) {
        temperature_list = data['temperature_list'];
        humidity_list = data['humidity_list'];
        wind_speed_list = data['wind_speed_list'];
        pressure_list = data['pressure_list'];
        temperature_list_date = data['temperature_list_date'];
        humidity_list_date = data['humidity_list_date'];
        wind_speed_list_date = data['wind_speed_list_date'];
        pressure_list_date = data['pressure_list_date'];

        drawGraph(temperature_list, humidity_list,wind_speed_list,pressure_list,
            temperature_list_date, humidity_list_date,wind_speed_list_date,pressure_list_date)

    })

}

function searchBtn() {
    var sensor_node_id = $('#sensor_node_id').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();
    var data;
    var temperature_list = [];
    var temperature_list_date = [];
    var humidity_list = [];
    var humidity_list_date = [];
    var wind_speed_list = [];
    var wind_speed_list_date = [];
    var pressure_list = [];
    var pressure_list_date = [];
    data = {
        'start_time': start_time,
        'end_time': end_time,
        'sensor_node_id': sensor_node_id
    }

    $.ajax({
        url: '/api/v1/filter_by_date/',
        dataType: 'json',
        data: JSON.stringify(data),
        type: 'POST',
        contentType: 'application/json;charset=UTF-8'
    }).success(function (data) {
       temperature_list = data['temperature_list'];
        humidity_list = data['humidity_list'];
        wind_speed_list = data['wind_speed_list'];
        pressure_list = data['pressure_list'];
        temperature_list_date = data['temperature_list_date'];
        humidity_list_date = data['humidity_list_date'];
        wind_speed_list_date = data['wind_speed_list_date'];
        pressure_list_date = data['pressure_list_date'];

        drawGraph(temperature_list, humidity_list,wind_speed_list,pressure_list,
            temperature_list_date, humidity_list_date,wind_speed_list_date,pressure_list_date)
    })


}

function drawGraph(temp_list, hum_list,wind_list,pressure_list, temp_list_date, hum_list_date,wind_list_date,pressure_list_date) {
    // bar chart data
    $('#temp_chart').remove();
    $('#hum_chart').remove();
    $('#wind_chart').remove();
    $('#pressure_chart').remove();
    var tempChart = document.createElement('canvas');
    tempChart.id = 'temp_chart';
    tempChart.width = 600;
    tempChart.height = 270;
    var humChart = document.createElement('canvas');
    humChart.id = 'hum_chart';
    humChart.width = 600;
    humChart.height = 270;
    var windChart = document.createElement('canvas');
    windChart.id = 'wind_chart';
    windChart.width = 600;
    windChart.height = 270;
    var pressureChart = document.createElement('canvas');
    pressureChart.id = 'pressure_chart';
    pressureChart.width = 600;
    pressureChart.height = 270;
    $('#temp_container').append(tempChart);
    $('#hum_container').append(humChart);
    $('#wind_container').append(windChart);
    $('#pressure_container').append(pressureChart);
    var tempData = {
        labels: temp_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: temp_list
            }]
    };
    var humData = {
        labels: hum_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: hum_list
            }]
    }
    var windData = {
        labels: wind_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: wind_list
            }]
    }
    var pressureData = {
        labels: pressure_list_date,
        datasets: [
            {
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                bezierCurve: false,
                data: pressure_list
            }]
    }

    Chart.defaults.global.animationSteps = 50;
    Chart.defaults.global.tooltipYPadding = 16;
    Chart.defaults.global.tooltipCornerRadius = 0;
    Chart.defaults.global.tooltipTitleFontStyle = "normal";
    Chart.defaults.global.tooltipFillColor = "rgba(0,0,0,0.8)";
    Chart.defaults.global.animationEasing = "easeOutBounce";
    Chart.defaults.global.responsive = false;
    Chart.defaults.global.scaleLineColor = "black";
    Chart.defaults.global.scaleFontSize = 16;

    // get bar chart canvas
    var tempChart = document.getElementById("temp_chart").getContext("2d");
    var humChart = document.getElementById("hum_chart").getContext("2d");
    var windChart = document.getElementById("wind_chart").getContext("2d");
    var pressureChart = document.getElementById("pressure_chart").getContext("2d");

    steps = 10
    var temp_max = 100;
    var hum_max = 100;
    // draw bar chart
    new Chart(tempChart).Line(tempData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(temp_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,
        bezierCurve: false

    });

    new Chart(humChart).Bar(humData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(hum_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,
        bezierCurve: false
    });
     new Chart(windChart).Bar(windData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(hum_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,
        bezierCurve: false
    });
      new Chart(pressureChart).Line(pressureData, {
        scaleOverride: true,
        scaleSteps: steps,
        scaleStepWidth: Math.ceil(hum_max / steps),
        scaleStartValue: 0,
        scaleShowVerticalLines: false,
        scaleShowGridLines: false,
        barShowStroke: true,
        scaleShowLabels: true,
        bezierCurve: false
    });
}

/**
 * Created by Nizomjon on 5/28/2017.
 */
