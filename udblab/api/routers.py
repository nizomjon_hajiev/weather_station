import binascii
import socket
import threading
import errno

from flask import Blueprint, jsonify, make_response, request, redirect, url_for, render_template
from flask_restful import abort
import pymysql.cursors
from udblab.models import *
from udblab.app import session, Message, mail, celery
from udblab import socket_client
import json
import time
from random import randint

mod = Blueprint('api', __name__, template_folder='site/templates')


@mod.route('/user/<int:user_id>', methods=['PUT'])
def get_user(user_id):
    json_dict = request.get_json()
    user = User.query.filter_by(id=user_id).first()
    locations = [i.serialize for i in Location.query.filter_by(owner_id=user.id).all()]
    response = jsonify({
        "email": user.email,
        "isAdmin": user.isAdmin,
        "username": user.username,
        "locations": locations,
        "json_dict": json_dict['test']
    })
    return make_response(response, 200)


@mod.route('/users/<int:user_id>/', methods=['GET'])
def get_user_values(user_id):
    user = User.query.filter_by(id=user_id).first()
    user_json = {}
    locations_json = []
    sensor_nodes_json = []
    sensor_models_json = []
    history_list_json = []
    locations = Location.query.filter_by(owner_id=user_id).all()
    for location in locations:
        sensor_nodes = SensorNode.query.filter_by(location_id=location.id).all()
        for sensor_node in sensor_nodes:
            sensor_model = SensorModel.query.filter_by(sensor_node_id=sensor_node.id).order_by(
                SensorModel.created_on.desc()).first()
            history_list = History.query.filter_by(sensor_node_id=sensor_node.id).order_by(
                History.end_time.desc()).all()
            for history in history_list:
                history_list_json.append(history.to_json())
            data = {
                'temp': '-3.74965517241',
                'rain': '0.0151724137931',
                'ws': '43',
                'humidity': '64.5931034483',
                'dp': '-9.87724137931',
                'sf': '0.215862068966',
                'max_sf': '2.0',
                'count_sf': '22'
            }

            sensor_nodes_json.append(sensor_node.to_json(sensor_model.to_json(), history_list_json, data))
            locations_json.append(location.to_json(sensor_nodes_json))

    response = {
        "username": user.username,
        "email": user.email,
        "isAdmin": user.isAdmin,
        "locations": locations_json,
    }

    return jsonify(response)


def test(test, mac_address):
    global client, mac_addr
    print('test')
    mac_addr = mac_address
    client = test
    print('mac_addr')
    print("Received client")
    return mac_address


mac_addr = None
start_command = True
client = None
should_retry = True


def checking_IO_controller(conn):
    global should_retry
    print("Connection")
    print(conn)
    if conn is not None:
        print(should_retry)
        while should_retry:
            try:
                conn.send(bytearray([0x53, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
                time.sleep(10)
                print("Send check command every 10s")
            except:
                should_retry = False
                print("Connection closed")


@mod.route('/check_mic_status/', methods=['GET'])
def check_mic_status():
    return jsonify(result=check_status_mic())


@mod.route('/open_connection/', methods=['GET'])
def open_connection():
    socket_client.start()
    return "Done"


@mod.route('/close_connection/', methods=['GET'])
def close_connection():
    # socket_client.close()
    return "Done"


@mod.route('/connected_client/', methods=['GET'])
def connected_client():
    global start_command, mc_status_list_start
    if request.method == 'GET':
        print(mac_addr)
        try:
            if mac_addr is not None:
                mac_address = mac_addr
                try:
                    client.send(bytearray([0x53, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x45]))
                    print("Start command send")
                    while start_command:
                        print("Listening data from start connection")
                        buf = client.recv(1024)
                        print(buf)
                        print("Start checking 30 second")
                        ## sending checking status every 30 second
                        # io_thread = threading.Thread(target=checking_IO_controller, args=(client,))
                        # io_thread.start()
                        # client.send(bytearray([0x53, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))
                        print("Data received ---> ")
                        # mc_status_list_start = receive_response(buf)
                        # print (mc_status_list_start)
                        print("--------------------")
                        start_command = False
                    print(mac_address)
                    data = {
                        'type': 'success',
                        "mac_address": mac_address
                    }
                    return jsonify(result=data)
                except socket.errno as e:
                    data = {
                        'type': 'error',
                        'message': e.message
                    }
                    return jsonify(result=data)

            else:
                data = {
                    'type': 'error'
                }
                return jsonify(result=data)
        except:
            data = {
                'type': 'error'
            }
            return jsonify(result=data)


def receive_response_on(buf):
    global mc_1
    print("Buffer ------------")
    print(buf)
    mc_status_list_data = []
    hexvalue = binascii.hexlify(buf).decode()
    list_buf = [hexvalue[i:i + 2] for i in range(0, len(hexvalue), 2)]
    unicoded_list = list_buf[2:5]
    mc_list = [str(i).strip() for i in unicoded_list]
    print("Mc_list __---------")
    print(mc_list)

    for mc in mc_list:
        if mc == '00':
            mc_1 = 0
        elif mc == '01':
            mc_1 = 1
        mc_status_list_data.append(mc_1)
    return mc_status_list_data


def receive_response_off(buf):
    global mc_1
    print("Buffer ------------")
    print(buf)
    mc_status_list_data = []
    hexvalue = binascii.hexlify(buf).decode()
    list_buf = [hexvalue[i:i + 2] for i in range(0, len(hexvalue), 2)]
    unicoded_list = list_buf[2:5]
    mc_list = [str(i).strip() for i in unicoded_list]
    print("Mc_list __---------")
    print(mc_list)

    for mc in mc_list:
        if mc == '00':
            mc_1 = 0
        elif mc == '01':
            mc_1 = 1
        mc_status_list_data.append(mc_1)
    return mc_status_list_data


def received_data_from_IO(buf):
    hexvalue = binascii.hexlify(buf).decode()
    list_buf = [hexvalue[i:i + 2] for i in range(0, len(hexvalue), 2)]
    unicoded_list = list_buf[2:5]
    return unicoded_list


# @socketio.on('connect', namespace='/test')
# def test_connect():
#     print('Client connected')


@mod.route('/heating/', methods=['POST'])
def heating():
    global data_on, data_off
    if request.method == 'POST':
        json_dict = request.get_json()
        type = json_dict['type']
        sensor_node_id = json_dict['sensor_node_id']
        if type == 'on':
            if client is not None:
                print(client)
                try:

                    client.send(bytearray([0x53, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x45]))
                    print("Client -----")
                    print(client)
                    # client.send(b'on')
                    print("On Command")
                    has_received_on = True
                    while has_received_on:
                        print("Listening data from client")
                        buf_on = client.recv(1024)
                        print(buf_on)
                        print("Data received ---> ")
                        mc_status_list_on = receive_response_on(buf_on)
                        data_from_io_on = received_data_from_IO(buf_on)
                        print(mc_status_list_on)
                        print("--------------------")
                        has_received_on = False
                        data_on = {
                            'type': 'success',
                            # 'mc_status_list_on': mc_status_list_on,
                            "data_from_io_on": data_from_io_on,
                        }
                    return jsonify(result=data_on)

                except socket.error:
                    data = {
                        'type': 'error',
                        # 'message': e.message
                    }
                    return jsonify(result=data)
            else:
                print("error turn on")
                data = {
                    'type': 'error',
                }
                return jsonify(result=data)
        elif type == 'off':

            if client is not None:
                print("Client -----")
                print(client)
                try:
                    client.send(bytearray([0x53, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45]))

                    # client.send(b'off')
                    print("Off Command")
                    has_received_off = True
                    while has_received_off:
                        print("Listening data from client")
                        buf_off = client.recv(1024)
                        print(buf_off)
                        print("Data received ---> ")
                        mc_status_list_off = receive_response_off(buf_off)
                        data_from_io_off = received_data_from_IO(buf_off)
                        data_off = {
                            'type': 'success',
                            # 'mc_status_list_off': mc_status_list_off,
                            "data_from_io_off": data_from_io_off,
                        }
                        print(mc_status_list_off)
                        print("--------------------")
                        has_received_off = False
                    start_time = json_dict['start_time']
                    end_time = json_dict['end_time']
                    calculated_time = json_dict['calculated_time']
                    aa = datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
                    bb = datetime.strptime(end_time, "%Y-%m-%d %H:%M:%S")
                    history = History(start_time=aa, end_time=bb, calculated_time=calculated_time,
                                      sensor_node_id=sensor_node_id)
                    db.session.add(history)
                    db.session.commit()
                    return jsonify(result=data_off)

                except socket.error:
                    data = {
                        'type': 'error'
                    }
                    return jsonify(result=data)

            else:
                print("error turn on")
                data = {
                    'type': 'error',
                }
                return jsonify(result=data)
    else:
        print("error turn on")
        data = {
            'type': 'error',
        }
        return jsonify(result=data)


@mod.route('/sensor_data_test/', methods=['POST'])
def sensor_data_test():
    test = request
    body_unicode = request.data.decode('utf-8')
    body = json.loads(body_unicode)
    humidity = body['humidity']
    temp = body['temp']
    wind_speed = body['wind_speed']
    unique_code = body['unique_code']

    sensor_node = SensorNode.query.filter_by(node_unique_id=unique_code).first()
    if sensor_node is not None:
        sensor_model = SensorModel(humidity=humidity, temperature=temp, wind_speed=wind_speed,
                                   sensor_node_id=sensor_node.id)
        db.session.add(sensor_model)
        db.session.commit()
        data = {
            'humidity': humidity,
            'temp': temp,
            'wind_speed': wind_speed,
            'unique_code': unique_code,
            'sensor_node_id': sensor_node.id,
            'sensor_model_id': sensor_model.id
        }
        return jsonify(result=data)
    else:
        return jsonify(result="error")


@mod.route('/analysis/', methods=['GET'])
def get_analysed():
    conn = pymysql.connect(host='localhost', user='wsadmin', password='wspass',
                           db='weather_station', charset='utf8')
    curs = conn.cursor()
    date = "01-01"
    sql = "select avg(temp) as temp, avg(rain) as rain, avg(ws) as ws, avg(humidity) as humidity, " \
          "avg(dp) as dp, avg(sf) as sf, max(sf) as msf " \
          "from history_snow_data where date_format(date, '%m-%d')='" + date + "'"
    curs.execute(sql)
    rows = curs.fetchall()
    print(rows)
    sql = "select count(sf) from history_snow_data where date_format(date, '%m-%d')='" + date + "' and sf > 0"
    curs.execute(sql)
    rows_a = curs.fetchall()
    print("(count sf) : \t\t" + str(rows_a[0][0]))
    conn.close()
    data = {
        'temp': str(rows[0][0]),
        'rain': str(rows[0][1]),
        'ws': str(rows[0][2]),
        'humidity': str(rows[0][3]),
        'dp': str(rows[0][4]),
        'sf': str(rows[0][5]),
        'max_sf': str(rows[0][6]),
        'count_sf': str(rows_a[0][0])
    }
    return jsonify(result=data)


@mod.route('/analysis_test/', methods=['GET'])
def test_analysis():
    data = {
        'temp': '-3.74965517241',
        'rain': '0.0151724137931',
        'ws': '43',
        'humidity': '64.5931034483',
        'dp': '-9.87724137931',
        'sf': '0.215862068966',
        'max_sf': '2.0',
        'count_sf': '22'
    }
    return jsonify(result=data)


@mod.route('/user/', methods=['POST'])
def create_user():
    json_dict = request.get_json()
    username = json_dict['username']
    email = json_dict['email']
    password = json_dict['password']
    isAdmin = json_dict['isAdmin']
    user = User(username=username, email=email, password=password, isAdmin=isAdmin)
    locations = (location for location in json_dict['locations'] if len(json_dict['locations']) > 0)
    for location in locations:
        location_db = Location(address=location['address'], owner_id=user)
        sensor_nodes = (sensor_node for sensor_node in location['sensor_nodes'] if len(location['sensor_nodes']) > 0)
        for sensor_node in sensor_nodes:
            sensor_node_db = SensorNode(latitude=sensor_node['latitude'],
                                        longitude=sensor_node['longitude'],
                                        location_id=location_db,
                                        status=True)
            sensors = (sensor for sensor in sensor_node['sensors'] if
                       len(sensor_node['sensors']) > 0)
            for sensor in sensors:
                sensor_type = (sensor['sensor_type'])
                sensor_db = Sensor(sensor_type=sensor_type,
                                   sensor_node_id=sensor_node_db)
                db.session.add(sensor_db)
            db.session.add(sensor_node_db)
        db.session.add(location_db)
    db.session.add(user)
    db.session.commit()
    return make_response('', 201)
    # for location in locations:


@mod.route('/user/<int:user_id>', methods=['DELETE'])
def delete_user(user_id):
    user = User.query.filter_by(id=user_id).first()
    if user is not None:
        locations = Location.query.filter_by(owner_id=user.id).all()
        if locations is not None and len(locations) > 0:
            for location in locations:
                sensor_nodes = SensorNode.query.filter_by(location_id=location.id).all()
                if sensor_nodes is not None and len(sensor_nodes) > 0:
                    for sensor_node in sensor_nodes:
                        sensors = Sensor.query.filter_by(sensor_node_id=sensor_node.id).all()
                        for sensor in sensors:
                            db.session.delete(sensor)
                        db.session.delete(sensor_node)
                    db.session.delete(location)
        db.session.delete(user)
        db.session.commit()
        data = {
            'result': 'deleted successfully'
        }
        return make_response(data, 204)
    else:
        abort(404, message='Invalid user')


@mod.route('/update/', methods=['PATCH'])
def update():
    json_dict = request.get_json()
    id = json_dict['id']
    username = json_dict['username']
    email = json_dict['email']
    isAdmin = json_dict['isAdmin']
    locations = (location for location in json_dict['locations'] if len(json_dict['locations']) > 0)
    for location in locations:
        location_id = location['id']
        location_address = location['address']
        sensor_nodes = (sensor_node for sensor_node in location['sensor_nodes'] if len(location['sensor_nodes']) > 0)
        for sensor_node in sensor_nodes:
            sensor_node_id = sensor_node['id']
            sensor_node_latitude = sensor_node['latitude']
            sensor_node_longitude = sensor_node['longitude']
            sensors = (sensor for sensor in sensor_node['sensors'] if
                       len(sensor_node['sensors']) > 0)
            for sensor in sensors:
                sensor_type = sensor['sensor_type']
                sensor_id = sensor['id']
                db_sensor = Sensor.query.filter_by(id=int(sensor_id)).first()
                if db_sensor is not None:
                    db_sensor.sensor_type = SensorTypeChecker.sensor_type_checker(sensor_type)
                    db_sensor.sensor_node_id = int(sensor_node_id)
                    db.session.commit()
                else:
                    return make_response(jsonify({'message': 'wrong sensor id : ' + str(sensor_id)}), 400)
            db_sensor_node = SensorNode.query.filter_by(id=int(sensor_node_id)).first()
            if db_sensor_node is not None:
                db_sensor_node.latitude = float(sensor_node_latitude)
                db_sensor_node.longitude = float(sensor_node_longitude)
                db_sensor_node.area_id = int(location_id)
                db.session.commit()
            else:
                return make_response(jsonify({'message': 'wrong sensor_node id : ' + str(sensor_node_id)}), 400)
        db_location = Location.query.filter_by(id=int(location_id)).first()
        if db_location is not None:
            db_location.address = location_address
            db_location.owner_id = int(id)
            db.session.commit()
        else:
            return make_response(jsonify({'message': 'wrong location_id : ' + str(location_id)}), 400)
    db_user = User.query.filter_by(id=int(id)).first()
    if db_user is not None:
        db_user.username = username
        db_user.email = email
        db_user.isAdmin = isAdmin
        db.session.commit()
        return make_response(jsonify({'message': 'success'}), 200)
    else:
        return make_response(jsonify({'message': 'Wrong id : ' + str(id)}), 400)


@mod.route('/login/', methods=['POST'])
def login():
    if request.method == 'POST':
        json_dict = request.get_json()
        email = json_dict['email']
        password = json_dict['password']
        user = User.query.filter_by(email=email).first()
        if user is not None and user.password == password:
            return jsonify(result=user.serialize), 200
        else:
            return jsonify(result='Invalid User'), 401


@mod.route('/signup/', methods=['POST'])
def signUp():
    if request.method == 'POST':
        json_dict = request.get_json()
        email = json_dict['email']
        password = json_dict['password']
        username = json_dict['username']
        dbUser = User.query.filter_by(email=email).first()
        if dbUser is None:
            user = User(username=username, email=email, password=password)
            db.session.add(user)
            db.session.commit()
            return jsonify(result=user.serialize), 200
        else:
            return jsonify(result='Already exist'), 401


@mod.route('/startup/')
def startup():
    total_users = len(User.query.all())
    total_locations = len(Location.query.all())
    chungju_locs = Location.query.filter(Location.address.like('%Chungju%')).all()
    seoul_locs = Location.query.filter(Location.address.like('%Seoul%')).all()
    chungju_loc_count = len(chungju_locs)
    seoul_loc_count = len(seoul_locs)

    response = jsonify({
        'total_users_count': total_users,
        'total_locations_count': total_locations,
        'chungju_loc_count': chungju_loc_count,
        'seoul_loc_count': seoul_loc_count
    })
    return make_response(response, 200)


@mod.route('/location', methods=['GET'])
def locations():
    query = request.args.get('q')
    if query == 'all':
        locations = Location.query.all()
    else:
        locations = Location.query.filter(Location.address.like('%' + query + '%')).all()

    data = [i.serialize for i in locations]

    return jsonify(data)


@mod.route('/user/')
def get_user1():
    user = session['user']  # superUser
    global user_id
    if user['isAdmin']:
        location_id = session['location_id']
        location = Location.query.filter(Location.id == location_id).first()
        user_id = location.owner_id
    else:
        user_id = user['id']
    user_data = User.query.filter(User.id == user_id).first()
    location = Location.query.filter(Location.owner_id == user_id).first()
    sensors = SensorNode.query.filter(SensorNode.location_id == location.id).all()
    location.sensors = sensors
    user_data.locations = [location]
    data = [i.serialize for i in user_data.locations]
    return jsonify(locations=data)


@mod.route('/user_view/')
def user_view():
    user = session['editable_user']
    user_id = user['id']
    user_data = User.query.filter(User.id == user_id).first()
    location = Location.query.filter(Location.owner_id == user_id).first()
    sensors = SensorNode.query.filter(SensorNode.area_id == location.id).all()
    location.sensors = sensors
    user_data.locations = [location]
    data = [i.serialize for i in user_data.locations]
    return jsonify(locations=data)


@mod.route('/password_reset', methods=['POST'])
def password_reset():
    json_dict = request.get_json()
    user_id = json_dict['user_id']
    user = User.query.filter(User.id == user_id).first()
    if user.id == int(user_id):
        if user.password == json_dict['current_password']:
            new_pwd = json_dict['new_password']
            user.password = int(new_pwd)
            db.session.commit()
            return jsonify({'result': 'success'})
        else:
            return jsonify({'result': 'error'})
    else:
        return jsonify({'result': 'error'})


@mod.route('/user_update', methods=['POST'])
def user_update():
    if request.method == "POST":
        id = request.form['id']
        user = User.query.filter_by(id=id).first()
        user.username = request.form['username']
        user.email = request.form['email']
        if request.args.get('user_role') == "General User":
            n_isAdmin = True
        else:
            n_isAdmin = False
        user.isAdmin = n_isAdmin
        locations = Location.query.filter(Location.owner_id == user.id).all()

        n_address_list = []
        aa = []
        more = True
        i = 1
        while more:
            if request.form.get('address_' + str(i)) and request.form.get('address_' + str(i)) != "":
                n_address_list.append(request.form.get('address_' + str(i)))
                locations[i - 1].address = request.form.get('address_' + str(i))
                more_sensor = True
                j = 1
                while more_sensor:
                    if request.form.get('latitude_' + str(j)) and request.form.get('longitude_' + str(j)) != "":
                        n_sensor_list = dict({'latitude': request.form.get('latitude_' + str(j)),
                                              "longitude": request.form.get('latitude_' + str(j)),
                                              "sensor_type": request.form.get('sensor_type_' + str(j))})
                        aa.append(n_sensor_list)
                        locations[i - 1].sensor_nodes[j - 1].latitude = request.form.get('latitude_' + str(j))
                        locations[i - 1].sensor_nodes[j - 1].longitude = request.form.get('longitude_' + str(j))

                    else:
                        more_sensor = False
                    j += 1
            else:
                more = False
            i += 1

        user.locations = locations
        db.session.commit()
        return redirect(url_for('site.user', id=id))


@mod.route('/attr_insert', methods=['POST', 'DELETE'])
def attr_insert():
    if request.method == 'POST':
        json_dict = request.get_json()
        if json_dict['type'] == 'sensor_node':
            sensor_node = SensorNode(latitude=json_dict['sensor_latitude'],
                                     longitude=json_dict['sensor_longitude'],
                                     location_id=json_dict['location_id'])
            db.session.add(sensor_node)
            db.session.commit()
            data = {
                'sensor_id': json_dict['id'],
                'latitude': json_dict['sensor_latitude'],
                'longitude': json_dict['sensor_longitude'],
                'sensor_node_id': str(sensor_node.id)

            }
            return jsonify({'result': data})
        elif json_dict['type'] == 'location':
            user_id = json_dict['user_id']
            address = json_dict['address']
            location = Location(address=address, owner_id=user_id)
            db.session.add(location)
            db.session.commit()
            data = {
                'location_id': location.id,
                'location_address': location.address
            }
            return jsonify({'result': data})
        elif json_dict['type'] == 'sensor':
            sesnor = Sensor(sensor_type='', sensor_node_id='')

    elif request.method == 'DELETE':
        json_dict = request.get_json()
        if json_dict['type'] == 'sensor':
            location_id = json_dict['location_id']
            sensor_id = json_dict['sensor_id']
            location = Location.query.filter_by(id=location_id).first()
            for sensor_node in location.sensor_nodes:
                if sensor_node.id == int(sensor_id):
                    for sensor in sensor_node.sensors:
                        db.session.delete(sensor)
                    db.session.delete(sensor_node)
                    db.session.commit()
            return jsonify({'result': 'success'})


        elif json_dict['type'] == 'location':
            location_id = json_dict['location_id']
            location = Location.query.filter_by(id=location_id).first()
            for sensor_node in location.sensor_nodes:
                for sensor in sensor_node.sensors:
                    db.session.delete(sensor)
                db.session.delete(sensor_node)
            db.session.delete(location)
            db.session.commit()
            return jsonify({'result': 'success'})
    else:
        return jsonify({'result': 'error'})


@mod.route('/last_seen/<int:node_id>/', methods=['GET'])
def last_seen_value(node_id):
    id = node_id
    sensor_node = SensorNode.query.filter_by(id=id).first()
    sensor_model = SensorModel.query.filter_by(sensor_node_id=sensor_node.id).order_by(
        SensorModel.created_on.desc()).first()

    return jsonify({
        'last_temp': sensor_model.humidity,
        'last_hum': sensor_model.temperature,
        'last_wind': sensor_model.wind_speed,
        'created_on': sensor_model.created_on
    })


@mod.route('/last_seen', methods=['POST', 'GET'])
def last_seen():
    if request.method == 'POST':
        json_dict = request.get_json()
        id = json_dict['id']
        sensor_node = SensorNode.query.filter_by(id=id).first()
        sensor_model = SensorModel.query.filter_by(sensor_node_id=sensor_node.id).order_by(
            SensorModel.created_on.desc()).first()

        return jsonify({
            'last_temp': sensor_model.humidity,
            'last_hum': sensor_model.temperature,
            'last_wind': sensor_model.wind_speed,
            'created_on': sensor_model.created_on
        })


@mod.route('/contact_us', methods=['POST'])
def contact_us():
    if request.method == 'POST':
        json_dict = request.get_json()
        name = json_dict['name']
        email = json_dict['email']
        phone = json_dict['phone']
        message = json_dict['message']

        send_template.delay()
        feedback = ContactUs(name=name, email=email, phone=phone, message=message)
        db.session.add(feedback)
        db.session.commit()
        data = {
            'name': name
        }
        return jsonify(data)


@mod.route('/feedback', methods=['POST'])
def feedback():
    if request.method == 'POST':
        json_dict = request.get_json()
        if json_dict['type'] == 'admin':
            feed_id = json_dict['feedback_id']
            response = json_dict['response']
            feedback = Feedback.query.filter_by(id=int(feed_id)).first()
            feedback.response = response
            db.session.commit()
            return jsonify(result='success')
        elif json_dict['type'] == 'user':
            user_id = json_dict['user_id']
            location_id = json_dict['location_id']
            sensor_node_id = json_dict['sensor_node_id']
            message = json_dict['message']
            feedback = Feedback(user_id=user_id, location_id=location_id,
                                sensor_node_id=sensor_node_id, message=message)
            db.session.add(feedback)
            db.session.commit()
            return jsonify(result='success')
        elif json_dict['type'] == 'general':
            user_id = json_dict['user_id']
            message = json_dict['message']
            location_id = json_dict['location_id']
            feedback = Feedback(user_id=user_id, location_id=location_id,
                                message=message)
            db.session.add(feedback)
            db.session.commit()
            return jsonify(result='success')


@mod.route('/admin_location_map/', methods=['POST'])
def admin_loc_map():
    json_dict = request.get_json()
    temperature_list = []
    temperature_list_date = []
    humidity_list = []
    humidity_list_date = []
    location_id = json_dict['location_id']
    location = Location.query.filter_by(id=location_id).first()
    sensor_nodes = SensorNode.query.filter_by(area_id=location.id).all()
    for sensor_node in sensor_nodes:
        for sensor in sensor_node.sensors:
            if sensor.sensor_type == SensorType.temperature:
                sensor_values = SensorValue.query.filter_by(sensor_id=sensor.id).all()
                for sensor_value in sensor_values:
                    temperature_list.append(sensor_value.value)
                    temperature_list_date.append(sensor_value.created_on.strftime('%y %m %d'))
            elif sensor.sensor_type == SensorType.humidity:
                sensor_values = SensorValue.query.filter_by(sensor_id=sensor.id).all()
                for sensor_value in sensor_values:
                    humidity_list.append(sensor_value.value)
                    humidity_list_date.append(sensor_value.created_on.strftime('%y %m %d'))

    return jsonify({
        'temperature_list': temperature_list,
        'humidity_list': humidity_list,
        'temperature_list_date': temperature_list_date,
        'humidity_list_date': humidity_list_date
    })


@mod.route('/admin_sensor_node/', methods=['POST'])
def admin_sensor_node():
    json_dict = request.get_json()
    temperature_list = []
    temperature_list_date = []
    humidity_list = []
    humidity_list_date = []
    wind_speed_list = []
    wind_speed_list_date = []
    pressure_list = []
    pressure_list_date = []
    sensor_node_id = json_dict['sensor_node_id']
    count = 12
    sensor_node = SensorNode.query.filter_by(id=sensor_node_id).first()
    for sensor in sensor_node.sensors:
        if sensor.sensor_type == SensorType.temperature:
            sensor_values = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).limit(count).all()
            for sensor_value in sensor_values:
                temperature_list.append(sensor_value.value)
                temperature_list_date.append(sensor_value.created_on.strftime('%y %m %d'))
        elif sensor.sensor_type == SensorType.humidity:
            sensor_values = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).limit(count).all()
            for sensor_value in sensor_values:
                humidity_list.append(sensor_value.value)
                humidity_list_date.append(sensor_value.created_on.strftime('%y %m %d'))
        elif sensor.sensor_type == SensorType.wind_speed:
            sensor_values = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).limit(count).all()
            for sensor_value in sensor_values:
                wind_speed_list.append(sensor_value.value)
                wind_speed_list_date.append(sensor_value.created_on.strftime('%y %m %d'))
        elif sensor.sensor_type == SensorType.pressure:
            sensor_values = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).limit(count).all()
            for sensor_value in sensor_values:
                pressure_list.append(sensor_value.value)
                pressure_list_date.append(sensor_value.created_on.strftime('%y %m %d'))
    return jsonify({
        'temperature_list': temperature_list,
        'humidity_list': humidity_list,
        'wind_speed_list': wind_speed_list,
        'pressure_list': pressure_list,
        'temperature_list_date': temperature_list_date,
        'humidity_list_date': humidity_list_date,
        'wind_speed_list_date': wind_speed_list_date,
        'pressure_list_date': pressure_list_date
    })


@mod.route('/filter_by_date/', methods=['POST'])
def filer_by_date():
    if request.method == 'POST':
        json_dict = request.get_json()
        temperature_list = []
        temperature_list_date = []
        humidity_list = []
        humidity_list_date = []
        wind_speed_list = []
        wind_speed_list_date = []
        pressure_list = []
        pressure_list_date = []
        end_time = json_dict['end_time']
        start_time = json_dict['start_time']
        sensor_node_id = json_dict['sensor_node_id']
        count = 12
        sensor_node = SensorNode.query.filter_by(id=int(sensor_node_id)).first()
        sensor_models = SensorModel.query.filter_by(sensor_node_id=sensor_node.id).filter(
            SensorModel.created_on.between(start_time, end_time)).order_by(
            SensorModel.created_on.desc()).limit(count).all()
        for sensor_model in sensor_models:
            temperature_list.append(sensor_model.temperature)
            temperature_list_date.append(sensor_model.created_on.strftime('%y %m %d'))
            humidity_list.append(sensor_model.humidity)
            humidity_list_date.append(sensor_model.created_on.strftime('%y %m %d'))
            wind_speed_list.append(sensor_model.wind_speed)
            wind_speed_list_date.append(sensor_model.created_on.strftime('%y %m %d'))

        return jsonify({
            'temperature_list': temperature_list,
            'humidity_list': humidity_list,
            'wind_speed_list': wind_speed_list,
            # 'pressure_list': pressure_list,
            'temperature_list_date': temperature_list_date,
            'humidity_list_date': humidity_list_date,
            'wind_speed_list_date': wind_speed_list_date,
            # 'pressure_list_date': pressure_list_date
        })


@mod.route('/node_sensor', methods=['GET'])
def sensor_node():
    id = request.args.get('id')
    sensor_node = SensorNode.query.filter_by(id=id).first()
    last_seen_values = []
    sensors = [i.serialize for i in Sensor.query.filter_by(sensor_node_id=sensor_node.id).all()]
    j = 0
    for sensor in range(len(sensors)):
        data = [i.serialize for i in SensorValue.query.filter_by(sensor_id=int(sensors[j]['id'])).all()]
        sensors[j]['sensor_values'] = data
        j += 1
    history = [i.serialize for i in
               History.query.filter_by(sensor_node_id=sensor_node.id).order_by(History.created_on.desc()).limit(
                   10).all()]
    for sensor in sensor_node.sensors:
        if sensor.sensor_type == SensorType.temperature:
            sensor_temp = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).first()
            last_seen_values.append(sensor_temp.serialize)
        elif sensor.sensor_type == SensorType.humidity:
            sensor_hum = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).first()
            last_seen_values.append(sensor_hum.serialize)
        elif sensor.sensor_type == SensorType.pressure:
            sensor_pressure = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).first()
            last_seen_values.append(sensor_pressure.serialize)
        elif sensor.sensor_type == SensorType.wind_speed:
            sensor_wind_speed = SensorValue.query.filter_by(sensor_id=sensor.id).order_by(
                SensorValue.created_on.desc()).first()
            last_seen_values.append(sensor_wind_speed.serialize)

    sensor_node_data = {
        'id': sensor_node.id,
        'location_id': sensor_node.location_id,
        'status': sensor_node.status,
        'latitude': sensor_node.latitude,
        'longitude': sensor_node.longitude,
        'sensors': sensors,
        'history': history,
        'last_seen_values': last_seen_values
    }
    return jsonify(sensor_node_data)


@mod.route('/history/', methods=['POST'])
def history():
    if request.method == 'POST':
        json_dict = request.get_json()
        if json_dict['type'] == 'post':
            sensor_node_id = json_dict['id']
            history = History.query.filter_by(sensor_node_id=sensor_node_id).order_by(
                History.end_time.desc()).all()
            data = [i.serialize for i in history]
            return jsonify(data)
        elif json_dict['type'] == 'get':
            json_dict = request.get_json()
            start_time = json_dict['start_time']
            end_time = json_dict['end_time']
            calculated_time = json_dict['calculated_time']
            sensor_node_id = json_dict['sensor_node_id']
            aa = datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
            bb = datetime.strptime(end_time, "%Y-%m-%d %H:%M:%S")
            history = History(start_time=aa, end_time=bb, calculated_time=calculated_time,
                              sensor_node_id=sensor_node_id)
            db.session.add(history)
            db.session.commit()
            return jsonify(result='success')


@mod.route('/sensor_data/', methods=['POST'])
def sensor_data():
    if request.method == 'POST':
        json_dict = request.get_json()
        sensor_node_unique_id = json_dict['unique_id']
        sensor_node_temperature = json_dict['temperature']
        sensor_node_humidity = json_dict['humidity']
        sensor_node_wind_speed = json_dict['wind_speed']
        sensor_node_camera_image = json_dict['image']


@celery.task(name='udblab.api.routers.send_template')
def send_template():
    msg = Message("Snow Melting", sender='nizom7812@gmail.com', recipients=['nizom7812@gmail.com'])
    msg.body = "Snow Melting"
    msg.html = render_template('email_template.html')
    mail.send(msg)
    print("sent")


def send_email(owner, email, response):
    msg = Message("Snow Melting Team", sender='nizom7812@gmail.com', recipients=[str(email)])
    msg.body = "Customer Service"
    msg.html = render_template('email_customer.html', owner=owner, response=response)
    mail.send(msg)


def generate_json_for_map(user_id):
    user_data = User.query.filter(User.id == user_id).first()
    locations = Location.query.filter(Location.owner_id == user_id).all()
    for location in locations:
        sensors = SensorNode.query.filter(SensorNode.area_id == location.id).all()
        location.sensors.append(sensors)

    # location.sensors = sensors
    user_data.locations = locations
    data = [i.serialize for i in user_data.locations]
    return jsonify(locations=data)


def check_status_mic():
    mic_list = [{
        'mic_1': 0
    }, {
        'mic_2': 1
    }, {
        'mic_3': 0
    }]
    return mic_list
