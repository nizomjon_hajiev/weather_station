import time

import os

from flask import Flask, session, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_script import Manager
from flask_restless import APIManager
from flask_restful import Resource, fields, marshal_with, Api
from flask_migrate import Migrate, MigrateCommand
from flask_mail import Mail, Message
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from random import randint
import threading
import radar
import binascii
# from tasks import *
from celery import Celery
from udblab import site, api
import random, socket, threading
from flask_socketio import SocketIO, emit
import errno
from threading import Lock

app = Flask(__name__)
# mail = Mail(app)
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'nizom7812@gmail.com'
app.config['MAIL_PASSWORD'] = 'nizom7812'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

mail = Mail(app)
async_mode = None
socketio = SocketIO(app, async_mode=async_mode)
from udblab.socket_test import *

thread = None
thread_lock = Lock()

api_app = Api(app)
app.secret_key = 's3cr3t'
app.config.from_pyfile('config.py')

client = []

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
Session = sessionmaker(bind=engine, autoflush=False)
session_db = Session()

celery = Celery("test", backend='amqp', broker='amqp://localhost//')

db = SQLAlchemy(app)
migrate = Migrate(app, db)
manager_app = Manager(app)
manager_app.add_command('db', MigrateCommand)
admin = Admin(app, template_mode='bootstrap3')
from udblab.models import *

db.create_all()

manager = APIManager(app, flask_sqlalchemy_db=db)

user_blueprint = manager.create_api(User, include_columns=['username', 'email', 'isAdmin', 'locations'],
                                    methods=['GET'])
location_blueprint = manager.create_api(Location, include_columns=['address'], methods=['GET'])
sensor_blueprint = manager.create_api(Sensor, include_columns=['latitude'])

api_app.add_resource(UserApi, '/api/v1/users/<user_id>')
api_app.add_resource(UserListApi, '/api/v1/users')
api_app.add_resource(LoctionApi, '/api/v1/locations/<location_id>/')
api_app.add_resource(LocationListApi, '/api/v1/locations/')
api_app.add_resource(SensorNodeListApi, '/api/v1/sensor_nodes/')
api_app.add_resource(SensorNodeApi, '/api/v1/sensor_nodes/<sensor_node_id>')
api_app.add_resource(SensorModelApi, '/api/v1/sensor_models/<sensor_node_id>/')
api_app.add_resource(FeedbackListApi, '/api/v1/feedbacks/')
api_app.add_resource(FeedbackApi, '/api/v1/feedbacks/<feedback_id>/')

sensor_type_list = ['temperature', 'humidity', 'pressure', 'wind_speed']

from udblab.api.routers import mod, test

from udblab.site.routers import mod

app.register_blueprint(site.routers.mod)
app.register_blueprint(api.routers.mod, url_prefix='/api/v1')


@socketio.on('On', namespace='/test')
def on_message(message):
    send_on_command()
    print("Heating On")
    # emit('my response', {'data': message['data']})


@socketio.on('Off', namespace='/test')
def off_message(message):
    send_off_command()
    print("Heating Off")
    # emit('my response', {'data': message['data']}, broadcast=True)


def background_thread():
    t = threading.Thread(target=run_server())
    t.daemon = True
    t.start()
    # start()


@socketio.on('connect', namespace='/test')
def connect():
    print("Connected")
    global thread
    with thread_lock:
        if thread is None:
            print("Starting background work")
            thread = threading.Thread(target=run_server())
            thread.daemon = True
            thread.start()
            # thread = socketio.start_background_task(target=background_thread)
            print("Started")
        else:
            print("Thread already started")
    print("Check device")
    data = {
        "type": "check_device",
        "data": check_connected_device()
    }
    emit('response', data, broadcast=True)


@socketio.on('disconnect', namespace='/test')
def disconnect():
    print('Client disconnected')
    raise Exception()
