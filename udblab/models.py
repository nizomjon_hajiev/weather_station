from datetime import datetime

from flask_restful import fields, Resource, marshal_with

from udblab.app import db
from flask import request, make_response, jsonify
from enum import Enum

db.create_all()

roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)

role_marshaller = {
    "name": fields.String,
    "description": fields.String
}

history_marshaller = {
    "start_time": fields.DateTime,
    "end_time": fields.DateTime,
    "calculated_time": fields.String
}

sensor_value_marshaller = {
    "id": fields.Integer,
    "sensor_id": fields.Integer,
    "updated_at": fields.DateTime(attribute='updated_on'),
    "value": fields.String,
    "type": fields.String
}

sensor_marshaller = {
    "id": fields.Integer,
    "sensor_type": fields.String,
    "sensor_node_id": fields.Integer,
    "sensor_values": fields.List(fields.Nested(sensor_value_marshaller))
}

sensors_model_marshaller = {
    "id": fields.Integer,
    "humidity": fields.Float,
    "temperature": fields.Float,
    "wind_speed": fields.Float,
    "sensor_node_id": fields.Integer,
    "created_on": fields.DateTime
}

sensor_nodes_marshaller = {
    "id": fields.Integer,
    "latitude": fields.String,
    "longitude": fields.String,
    "location_id": fields.Integer,
    "status": fields.Boolean,
    'node_unique_id': fields.String,
    "sensor_models": fields.List(fields.Nested(sensors_model_marshaller)),
    "history": fields.List(fields.Nested(history_marshaller)),
}

location_marshaller = {
    "id": fields.Integer,
    "area_code": fields.Integer,
    'user_id': fields.Integer(attribute='owner_id'),
    "address": fields.String,
    "postal_code": fields.Integer,
    "camera_url": fields.String(default=''),
    "road_address": fields.String,
    "sensor_nodes": fields.List(fields.Nested(sensor_nodes_marshaller))
}

user_marshaller = {
    "id": fields.Integer,
    "username": fields.String,
    "email": fields.String,
    "isAdmin": fields.Boolean,
    "locations": fields.List(fields.Nested(location_marshaller))
}

list_user_marshaller = {
    "id": fields.Integer,
    "username": fields.String,
    "email": fields.String,
    "isAdmin": fields.Boolean,
}

list_location_marshaller = {
    "id": fields.Integer,
    "area_code": fields.Integer,
    'user_id': fields.Integer(attribute='owner_id'),
    "address": fields.String,
    "postal_code": fields.Integer,
    "camera_url": fields.String(default=''),
    "road_address": fields.String
}

list_sensor_node_marshaller = {
    "id": fields.Integer,
    "latitude": fields.String,
    "longitude": fields.String,
    "location_id": fields.Integer,
    "status": fields.Boolean,
    'node_unique_id': fields.String,
}

list_feedback_marshaller = {
    'id': fields.Integer,
    'user_id': fields.Integer,
    'location_id': fields.Integer,
    'sensor_node_id': fields.Integer,
    'message': fields.String,
    'response': fields.String
}


# def abort_if_user_doesnt_exist(user_id):

class SensorModelCustom(fields.Raw):
    def output(self, key, obj):
        sensor_node = SensorNode.query.filter_by(id=id).first()
        sensor_model = SensorModel.query.filter_by(sensor_node_id=sensor_node.id).order_by(
            SensorModel.created_on.desc()).first()
        return jsonify({
            'last_temp': sensor_model.humidity,
            'last_hum': sensor_model.temperature,
            'last_wind': sensor_model.wind_speed,
            'created_on': sensor_model.created_on
        })


class UserApi(Resource):
    @marshal_with(user_marshaller)
    def get(self, user_id):
        user = User.query.get(user_id)
        return user

    @marshal_with(user_marshaller)
    def put(self):
        user_id = request.form['test']
        return 'User id : ' + user_id

    @marshal_with(user_marshaller)
    def delete(self, user_id):
        user = User.query.get(user_id)
        if user is not None:
            locations = Location.query.filter_by(owner_id=user.id).all()
            for location in locations:
                sensor_nodes = SensorNode.query.filter_by(location_id=location.id).all()
                for sensor_node in sensor_nodes:
                    sensor_model = SensorModel.query.filter_by(sensor_node_id=sensor_node.id).all()
                    for sensor_model in sensor_model:
                        db.session.delete(sensor_model)
                    db.session.delete(sensor_node)
                db.session.delete(location)
            db.session.delete(user)
            db.session.commit()
            data = {
                'result': 'deleted successfully'
            }
            return jsonify(results='deleted successfully'), 204
        else:
            data = {
                'result': 'invalid user'
            }
            return jsonify(result='invalid user'), 404


class LoctionApi(Resource):
    @marshal_with(location_marshaller)
    def get(self, location_id):
        location = Location.query.get(location_id)
        return location


class UserListApi(Resource):
    @marshal_with(list_user_marshaller)
    def get(self):
        user = User.query.all()
        return user


class LocationListApi(Resource):
    @marshal_with(list_location_marshaller)
    def get(self):
        locations = Location.query.all()
        return locations


class SensorNodeApi(Resource):
    @marshal_with(sensor_nodes_marshaller)
    def get(self, sensor_node_id):
        sensor_node = SensorNode.query.get(sensor_node_id)
        return sensor_node


class SensorNodeListApi(Resource):
    @marshal_with(list_sensor_node_marshaller)
    def get(self):
        sensor_nodes = SensorNode.query.all()
        return sensor_nodes


class SensorModelApi(Resource):
    @marshal_with(sensors_model_marshaller)
    def get(self, sensor_node_id):
        sensor_model = SensorModel.query.filter_by(sensor_node_id=sensor_node_id).order_by(
            SensorModel.created_on.desc()).first()
        print(sensor_node_id)
        return sensor_model


class FeedbackListApi(Resource):
    @marshal_with(list_feedback_marshaller)
    def get(self):
        feedback = Feedback.query.all()
        return feedback


class FeedbackApi(Resource):
    @marshal_with(list_feedback_marshaller)
    def get(self, feedback_id):
        feedback = Feedback.query.get(feedback_id)
        return feedback

    @marshal_with(list_feedback_marshaller)
    def put(self, feedback_id):
        feedback = Feedback.query.get(feedback_id)
        json_dict = request.get_json()
        return feedback


class Base(db.Model):
    __abstract__ = True
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class Role(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name


class User(Base):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(50))
    isAdmin = db.Column(db.Boolean)
    locations = db.relationship('Location', backref='owner', lazy='dynamic')
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        return self.email

    @property
    def serialize(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'isAdmin': self.isAdmin,
            'locations': [i.serialize for i in self.locations]
        }


class Location(Base):
    __tablename__ = 'location'
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(400))
    road_address = db.Column(db.String(400))
    camera_url = db.Column(db.String(50))
    owner_id = db.Column(db.Integer, db.ForeignKey(User.id))
    postal_code = db.Column(db.Integer)
    sensor_nodes = db.relationship('SensorNode', backref='area', lazy='dynamic')

    def __str__(self):
        return self.address

    def getOwner(self, owner_id):
        return User.query.filter_by(id=owner_id).first()

    def getSensorNode(self, location_id):
        return SensorNode.query.filter_by(location_id=location_id).first()

    def to_json(self, sensor_nodes):
        return {
            'id': self.id,
            'address': self.address,
            'road_address': self.road_address,
            'camera_url': self.camera_url,
            'user_id': self.owner_id,
            'postal_code': self.postal_code,
            'sensor_nodes': sensor_nodes
        }

    @property
    def serialize(self):
        return {
            'id': self.id,
            'address': self.address,
            'road_address': self.road_address,
            'user_id': self.owner_id,
            'area_code': self.postal_code,
            'sensor_nodes': [i.serialize for i in self.sensor_nodes]
        }


class SensorType(Enum):
    temperature = 'temperature'
    humidity = 'humidity'
    pressure = 'pressure'
    wind_speed = 'wind_speed'


class SensorTypeChecker:
    @staticmethod
    def sensor_type_checker(sensor_type):
        if sensor_type == 'temperature':
            return SensorType.temperature
        elif sensor_type == 'humidity':
            return SensorType.humidity
        elif sensor_type == 'pressure':
            return SensorType.pressure
        elif sensor_type == 'wind_speed':
            return SensorType.wind_speed


class SensorNode(Base):
    id = db.Column(db.Integer, primary_key=True)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    location_id = db.Column(db.Integer, db.ForeignKey(Location.id))
    node_unique_id = db.Column(db.String(100))
    tcp = db.Column(db.String)
    status = db.Column(db.Boolean, default=True)
    sensor_models = db.relationship('SensorModel', backref='sensor_node', lazy='dynamic')
    sensors = db.relationship('Sensor', backref='sensor_node', lazy='dynamic')
    history = db.relationship('History', backref='sensor_node', lazy='dynamic')

    def __str__(self):
        return str(self.sensor_type)

    def to_json(self, sensor_model, history, analysis):
        return {
            'id': self.id,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'node_unique_id': self.node_unique_id,
            'last_seen_values': sensor_model,
            'analysis': analysis,
            'history': history
        }

    @property
    def serialize(self):
        return {
            'sensor_node_id': self.id,
            'node_unique_id': self.node_unique_id,
            'location_id': self.location_id,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'status': self.status
        }


class SensorModel(Base):
    id = db.Column(db.Integer, primary_key=True)
    humidity = db.Column(db.Float)
    temperature = db.Column(db.Float)
    wind_speed = db.Column(db.Float)
    sensor_node_id = db.Column(db.Integer, db.ForeignKey(SensorNode.id))

    def to_json(self):
        return {
            'id': self.id,
            'humidity': self.humidity,
            'temperature': self.temperature,
            'wind_speed': self.wind_speed,
        }


class Sensor(Base):
    id = db.Column(db.Integer, primary_key=True)
    sensor_type = db.Column(db.Enum(SensorType))
    sensor_type_id = db.Column(db.Integer)
    sensor_node_id = db.Column(db.Integer, db.ForeignKey(SensorNode.id))
    sensor_values = db.relationship('SensorValue', backref='sensor', lazy='dynamic')

    def get_sensor_type(self, sensor_type):
        if sensor_type == SensorType.temperature:
            return SensorType.temperature.name
        elif sensor_type == SensorType.humidity:
            return SensorType.humidity.name
        elif sensor_type == SensorType.pressure:
            return SensorType.pressure.name
        elif sensor_type == SensorType.wind_speed:
            return SensorType.wind_speed.name

    @property
    def serialize(self):
        return {
            'id': self.id,
            'sensor_node_id': self.sensor_node_id,
            'sensor_type': str(self.sensor_type)
        }


class SensorValue(Base):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String)
    type = db.Column(db.String)
    sensor_id = db.Column(db.Integer, db.ForeignKey(Sensor.id))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'updated_at': self.updated_on,
            'value': self.value,
            'type': self.type,
            'sensor_id': self.sensor_id
        }

        # class SensorTemperature(Base):


# id = db.Column(db.Integer, primary_key=True)
#     value = db.Column(db.String(40))
#     sensor_id = db.Column(db.Integer, db.ForeignKey(Sensor.id))
#
#
# class SensorHumidity(Base):
#     id = db.Column(db.Integer, primary_key=True)
#     value = db.Column(db.String(40))
#     sensor_id = db.Column(db.Integer, db.ForeignKey(Sensor.id))
#
#
# class SensorPressure(Base):
#     id = db.Column(db.Integer, primary_key=True)
#     value = db.Column(db.String(40))
#     sensor_id = db.Column(db.Integer, db.ForeignKey(Sensor.id))
#
#
# class SensorWind(Base):
#     id = db.Column(db.Integer, primary_key=True)
#     value = db.Column(db.String(40))
#     sensor_id = db.Column(db.Integer, db.ForeignKey(Sensor.id))


class History(Base):
    id = db.Column(db.Integer, primary_key=True)
    start_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)
    calculated_time = db.Column(db.String)
    sensor_node_id = db.Column(db.Integer, db.ForeignKey(SensorNode.id))

    def to_json(self):
        return {
            'id': self.id,
            'start_time': self.start_time,
            'end_time': self.end_time,
            'calculated_time': self.calculated_time,
        }

    @property
    def serialize(self):
        return {
            'id': self.id,
            'start_time': self.start_time,
            'end_time': self.end_time,
            'calculated_time': self.calculated_time,
            'sensor_node_id': self.sensor_node_id
        }

    def __str__(self):
        return 'start_time : ' + self.start_time + '\n' + 'end_time : ' + self.end_time


class ContactUs(Base):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    email = db.Column(db.String(30))
    phone = db.Column(db.String(30))
    message = db.Column(db.String(400))


class RecentActivities(Base):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    location_id = db.Column(db.Integer)
    sensor_node_id = db.Column(db.Integer)
    sensor_id = db.Column(db.Integer)
    feedback_id = db.Column(db.Integer)
    value = db.Column(db.String(100))
    type = db.Column(db.String(50))


class Feedback(Base):
    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.VARCHAR(500))
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    location_id = db.Column(db.Integer, db.ForeignKey(Location.id))
    sensor_node_id = db.Column(db.Integer, db.ForeignKey(SensorNode.id))
    response = db.Column(db.VARCHAR(500))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'message': self.message,
            'user_id': self.user_id,
            'location_id': self.location_id,
            'sensor_node_id': self.sensor_node_id,
            'response': self.response
        }


class AreaCode(Base):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.VARCHAR(100))
    code = db.Column(db.Integer)
